USE [master]
GO
/****** Object:  Database [PersoanlSecurity]    Script Date: 12/12/2017 1:58:41 PM ******/
CREATE DATABASE [PersoanlSecurity]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PersoanlSecurity', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\PersoanlSecurity.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PersoanlSecurity_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\PersoanlSecurity_log.ldf' , SIZE = 1280KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PersoanlSecurity].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PersoanlSecurity] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET ARITHABORT OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PersoanlSecurity] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PersoanlSecurity] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PersoanlSecurity] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PersoanlSecurity] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PersoanlSecurity] SET  MULTI_USER 
GO
ALTER DATABASE [PersoanlSecurity] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PersoanlSecurity] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PersoanlSecurity] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PersoanlSecurity] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [PersoanlSecurity]
GO
/****** Object:  Table [dbo].[AclActionRecords]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AclActionRecords](
	[AclActionRecordId] [int] IDENTITY(1,1) NOT NULL,
	[AclControllerRecordId] [int] NOT NULL,
	[AclActionName] [nvarchar](250) NOT NULL,
	[AclActionLabel] [nvarchar](250) NOT NULL,
	[IsDefault] [bit] NOT NULL,
 CONSTRAINT [PK_AccActionRecords] PRIMARY KEY CLUSTERED 
(
	[AclActionRecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AclControllerRecords]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AclControllerRecords](
	[AclControllerRecordId] [int] IDENTITY(1,1) NOT NULL,
	[AclAreaRecordId] [int] NULL,
	[AclControllerName] [nvarchar](250) NOT NULL,
	[AclControllerLabel] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_AclControllerRecords] PRIMARY KEY CLUSTERED 
(
	[AclControllerRecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DepartmentRecords]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DepartmentRecords](
	[DepartmentRecordsId] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentNameEng] [nvarchar](250) NULL,
	[DepartmentNameNep] [nvarchar](250) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_DepartmentRecords] PRIMARY KEY CLUSTERED 
(
	[DepartmentRecordsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DesignationRecords]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DesignationRecords](
	[DesignationSetupId] [int] IDENTITY(1,1) NOT NULL,
	[DesignationNameEng] [nvarchar](250) NOT NULL,
	[DesignationNameNep] [nvarchar](250) NOT NULL,
	[Status] [bit] NOT NULL,
	[DesignationFor] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [date] NOT NULL,
	[ModifiedDate] [date] NOT NULL,
 CONSTRAINT [PK_DesignationRecords] PRIMARY KEY CLUSTERED 
(
	[DesignationSetupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmployeeRecords]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeRecords](
	[EmployeeId] [int] IDENTITY(1,1) NOT NULL,
	[FullNameEng] [nvarchar](250) NOT NULL,
	[FullNameNep] [nvarchar](250) NOT NULL,
	[DateOfBirth] [date] NOT NULL,
	[MartialStatus] [int] NOT NULL,
	[GenderId] [int] NOT NULL,
	[DesignationId] [int] NOT NULL,
	[MobileNumber] [nvarchar](50) NULL,
	[ContactNumber] [nvarchar](250) NULL,
	[DepartmentId] [int] NOT NULL,
	[Address] [nvarchar](350) NOT NULL,
	[Photo] [nvarchar](350) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [date] NOT NULL,
	[Status] [bit] NOT NULL,
	[UpdatedDate] [date] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_EmployeeRecords] PRIMARY KEY CLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GenderRecords]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GenderRecords](
	[GenderId] [int] IDENTITY(1,1) NOT NULL,
	[GenderEnglish] [nvarchar](50) NOT NULL,
	[GenderNepali] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_GenderRecords] PRIMARY KEY CLUSTERED 
(
	[GenderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MartialStatusRecords]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MartialStatusRecords](
	[MartialStatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusNameEng] [nvarchar](150) NOT NULL,
	[StatusNameNep] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_MartialStatusRecords] PRIMARY KEY CLUSTERED 
(
	[MartialStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProvincesDistrictSetup]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProvincesDistrictSetup](
	[ProvincesDistrictId] [int] IDENTITY(1,1) NOT NULL,
	[ReferenceDistrictId] [int] NULL,
	[DistrictName] [nvarchar](250) NOT NULL,
	[ProvincesId] [int] NOT NULL,
 CONSTRAINT [PK_ProvincesDistrictSetup] PRIMARY KEY CLUSTERED 
(
	[ProvincesDistrictId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[provincesSetup]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[provincesSetup](
	[provincesId] [int] IDENTITY(1,1) NOT NULL,
	[provincesName] [nvarchar](250) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_provincesSetup] PRIMARY KEY CLUSTERED 
(
	[provincesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[provincesVDCDetails]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[provincesVDCDetails](
	[ProvincesVDCDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[VDCName] [nvarchar](200) NOT NULL,
	[ProvincesId] [int] NOT NULL,
	[ProvincesDistrictId] [int] NOT NULL,
	[VdcOrMunicipilityTypeId] [int] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_provincesVDCDetails] PRIMARY KEY CLUSTERED 
(
	[ProvincesVDCDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProfile](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](56) NOT NULL,
	[UserType] [int] NULL,
	[UserGroup] [int] NULL,
	[EmployeeId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserType]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserType](
	[UserTypeId] [int] IDENTITY(1,1) NOT NULL,
	[UserTypeName] [nvarchar](250) NOT NULL,
	[IsApproved] [bit] NOT NULL,
	[CreatedDate] [date] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedDate] [date] NOT NULL,
 CONSTRAINT [PK_UserType] PRIMARY KEY CLUSTERED 
(
	[UserTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[webpages_Membership]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Membership](
	[UserId] [int] NOT NULL,
	[CreateDate] [datetime] NULL,
	[ConfirmationToken] [nvarchar](128) NULL,
	[IsConfirmed] [bit] NULL DEFAULT ((0)),
	[LastPasswordFailureDate] [datetime] NULL,
	[PasswordFailuresSinceLastSuccess] [int] NOT NULL DEFAULT ((0)),
	[Password] [nvarchar](128) NOT NULL,
	[PasswordChangedDate] [datetime] NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[PasswordVerificationToken] [nvarchar](128) NULL,
	[PasswordVerificationTokenExpirationDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[webpages_OAuthMembership]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_OAuthMembership](
	[Provider] [nvarchar](30) NOT NULL,
	[ProviderUserId] [nvarchar](100) NOT NULL,
	[UserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Provider] ASC,
	[ProviderUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[webpages_Roles]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[webpages_UsersInRoles]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpages_UsersInRoles](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AclActionRecords] ON 

INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (1, 1, N'Login', N'Employee Login', 1)
INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (2, 1, N'Register', N'Employee Creation', 1)
INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (3, 2, N'Create', N'New Employee Create', 1)
INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (4, 2, N'Edit', N'Update Employee', 1)
INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (5, 2, N'index', N'List Employee', 1)
INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (7, 2, N'Delete', N'Delete Employee', 1)
INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (8, 3, N'Create', N'User Type Creation', 1)
INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (9, 3, N'Edit', N'Update User Type', 1)
INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (10, 3, N'index', N'List User Type', 1)
INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (11, 3, N'Delete', N'Delete User Type', 1)
INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (12, 4, N'Create', N'Designation Creation', 1)
INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (13, 4, N'Edit', N'Update Designation', 1)
INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (14, 4, N'index', N'List Designation', 1)
INSERT [dbo].[AclActionRecords] ([AclActionRecordId], [AclControllerRecordId], [AclActionName], [AclActionLabel], [IsDefault]) VALUES (15, 4, N'Delete', N'Delete Designation', 1)
SET IDENTITY_INSERT [dbo].[AclActionRecords] OFF
SET IDENTITY_INSERT [dbo].[AclControllerRecords] ON 

INSERT [dbo].[AclControllerRecords] ([AclControllerRecordId], [AclAreaRecordId], [AclControllerName], [AclControllerLabel]) VALUES (1, NULL, N'Account', N'User Account')
INSERT [dbo].[AclControllerRecords] ([AclControllerRecordId], [AclAreaRecordId], [AclControllerName], [AclControllerLabel]) VALUES (2, NULL, N'Employee', N'Employee Records')
INSERT [dbo].[AclControllerRecords] ([AclControllerRecordId], [AclAreaRecordId], [AclControllerName], [AclControllerLabel]) VALUES (3, NULL, N'UserType', N'UserType Records')
INSERT [dbo].[AclControllerRecords] ([AclControllerRecordId], [AclAreaRecordId], [AclControllerName], [AclControllerLabel]) VALUES (4, NULL, N'DesignationSetup', N'Designation Records')
INSERT [dbo].[AclControllerRecords] ([AclControllerRecordId], [AclAreaRecordId], [AclControllerName], [AclControllerLabel]) VALUES (5, NULL, N'DepartmentRecord', N'Department Records')
SET IDENTITY_INSERT [dbo].[AclControllerRecords] OFF
SET IDENTITY_INSERT [dbo].[DepartmentRecords] ON 

INSERT [dbo].[DepartmentRecords] ([DepartmentRecordsId], [DepartmentNameEng], [DepartmentNameNep], [Status]) VALUES (1, N'IT', N'IT', NULL)
INSERT [dbo].[DepartmentRecords] ([DepartmentRecordsId], [DepartmentNameEng], [DepartmentNameNep], [Status]) VALUES (2, N'Administrator', N'Administrator', NULL)
SET IDENTITY_INSERT [dbo].[DepartmentRecords] OFF
SET IDENTITY_INSERT [dbo].[DesignationRecords] ON 

INSERT [dbo].[DesignationRecords] ([DesignationSetupId], [DesignationNameEng], [DesignationNameNep], [Status], [DesignationFor], [CreatedBy], [CreatedDate], [ModifiedDate]) VALUES (1, N'Manager', N'Manager', 1, 0, 0, CAST(N'0001-01-01' AS Date), CAST(N'0001-01-01' AS Date))
SET IDENTITY_INSERT [dbo].[DesignationRecords] OFF
SET IDENTITY_INSERT [dbo].[GenderRecords] ON 

INSERT [dbo].[GenderRecords] ([GenderId], [GenderEnglish], [GenderNepali]) VALUES (1, N'Male', N'पुरूष')
INSERT [dbo].[GenderRecords] ([GenderId], [GenderEnglish], [GenderNepali]) VALUES (2, N'Female', N'महिला')
INSERT [dbo].[GenderRecords] ([GenderId], [GenderEnglish], [GenderNepali]) VALUES (3, N'Other', N'अन्य')
SET IDENTITY_INSERT [dbo].[GenderRecords] OFF
SET IDENTITY_INSERT [dbo].[MartialStatusRecords] ON 

INSERT [dbo].[MartialStatusRecords] ([MartialStatusId], [StatusNameEng], [StatusNameNep]) VALUES (1, N'Married', N'विवाहित')
INSERT [dbo].[MartialStatusRecords] ([MartialStatusId], [StatusNameEng], [StatusNameNep]) VALUES (2, N'Unmarried', N'अविवाहित')
INSERT [dbo].[MartialStatusRecords] ([MartialStatusId], [StatusNameEng], [StatusNameNep]) VALUES (3, N'Ohter', N'अन्य')
SET IDENTITY_INSERT [dbo].[MartialStatusRecords] OFF
SET IDENTITY_INSERT [dbo].[ProvincesDistrictSetup] ON 

INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (1, 1, N'ताप्लेजुङ', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (2, 2, N'पाँचथर', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (3, 3, N'ईलाम', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (4, 9, N'संखुवासभा', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (5, 8, N'तेर्हथुम ', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (6, 7, N'धनकुटा ', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (8, 10, N'भोजपुर', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (9, 13, N'खोटांङ', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (10, 11, N'सोलुखुम्बु', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (11, 12, N'ओखलढुंगा', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (12, 14, N'उदयपुर', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (13, 4, N'झापा ', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (14, 5, N'मोरङ', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (15, 6, N'सुनसरी ', 1)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (16, 15, N'सप्तरी ', 2)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (17, 16, N'सिराहा', 2)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (18, 17, N'धनुषा ', 2)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (19, 18, N'महोतरी ', 2)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (20, 19, N'सर्लाही ', 2)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (21, 32, N'रौतहट ', 2)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (22, 33, N'बारा ', 2)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (23, 34, N'पर्सा ', 2)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (24, 22, N'दोलखा ', 3)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (25, 21, N'रामेछाप ', 3)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (26, 20, N'सिन्धुली ', 3)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (27, 24, N'काभ्रेपलान्चोक ', 3)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (28, 23, N'सिन्धुपाल्चोक', 3)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (29, 29, N'रसुवा ', 3)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (30, 28, N'नुवाकोट ', 3)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (31, 30, N'धादिंङ ', 3)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (32, 35, N'चितवन ', 3)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (33, 26, N'भक्तपुर ', 3)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (34, 31, N'मकवानपुर ', 3)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (35, 25, N'ललितपुर', 3)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (36, 27, N'काठमाण्डौ', 3)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (37, 36, N'गोर्खा', 4)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (38, 37, N'लमजुङ', 4)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (39, 38, N'तनहुँ', 4)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (40, 40, N'कास्की', 4)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (41, 41, N'मनाङ', 4)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (42, 42, N'मुस्तांङ', 4)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (43, 44, N'पर्वत', 4)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (44, 39, N'स्याग्जा', 4)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (45, 43, N'म्याग्दी', 4)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (46, 45, N'बाग्लुंङ', 4)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (47, 48, N'नवलपरासी (बरद्घाट सुस्तापूर्व)', 4)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (48, 48, N'नवलपरासी (बरद्घाट सुस्ता पश्चिम )', 5)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (49, 49, N'रुपन्देही', 5)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (50, 50, N'कपिलवस्तु', 5)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (51, 47, N'पाल्पा', 5)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (52, 51, N'अर्घाखाँची', 5)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (53, 46, N'गुल्मी', 5)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (54, 54, N'रुकुम (पूर्वी भाग)', 5)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (55, 53, N'रोल्पा ', 5)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (56, 52, N'प्युठान ', 5)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (57, 56, N'दाङ', 5)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (58, 57, N'बाँके ', 5)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (59, 58, N'बर्दिया ', 5)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (60, 54, N'रुकुम (पश्चिम भाग)', 6)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (61, 55, N'सल्यान ', 6)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (62, 62, N'डोल्पा ', 6)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (63, 63, N'जुम्ला ', 6)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (64, 65, N'मुगु ', 6)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (65, 66, N'हुम्ला ', 6)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (66, 64, N'कालिकोट ', 6)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (67, 61, N'जाजरकोट', 6)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (68, 60, N'दैलेख ', 6)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (69, 59, N'सुर्खेत ', 6)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (70, 67, N'बाजुरा ', 7)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (71, 68, N'बझाङ', 7)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (72, 70, N'डोटी ', 7)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (73, 69, N'अछाम', 7)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (74, 75, N'दार्चुला ', 7)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (75, 74, N'बैतडी ', 7)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (76, 73, N'डडेलधुरा ', 7)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (77, 72, N'कञ्चनपुर', 7)
INSERT [dbo].[ProvincesDistrictSetup] ([ProvincesDistrictId], [ReferenceDistrictId], [DistrictName], [ProvincesId]) VALUES (78, 71, N'कैलाली', 7)
SET IDENTITY_INSERT [dbo].[ProvincesDistrictSetup] OFF
SET IDENTITY_INSERT [dbo].[provincesSetup] ON 

INSERT [dbo].[provincesSetup] ([provincesId], [provincesName], [Status]) VALUES (1, N'प्रदेश नम्बर १', 1)
INSERT [dbo].[provincesSetup] ([provincesId], [provincesName], [Status]) VALUES (2, N'प्रदेश नम्बर २', 1)
INSERT [dbo].[provincesSetup] ([provincesId], [provincesName], [Status]) VALUES (3, N'प्रदेश नम्बर ३', 1)
INSERT [dbo].[provincesSetup] ([provincesId], [provincesName], [Status]) VALUES (4, N'प्रदेश नम्बर ४', 1)
INSERT [dbo].[provincesSetup] ([provincesId], [provincesName], [Status]) VALUES (5, N'प्रदेश नम्बर ५', 1)
INSERT [dbo].[provincesSetup] ([provincesId], [provincesName], [Status]) VALUES (6, N'प्रदेश नम्बर ६', 1)
INSERT [dbo].[provincesSetup] ([provincesId], [provincesName], [Status]) VALUES (7, N'प्रदेश नम्बर ७', 1)
SET IDENTITY_INSERT [dbo].[provincesSetup] OFF
SET IDENTITY_INSERT [dbo].[provincesVDCDetails] ON 

INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (1, N'फुङलिङ', 1, 1, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (2, N'आठराई त्रिवेणी', 1, 1, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (3, N'सिदिङ्वा ', 1, 1, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (4, N'फक्ताङलुङ ', 1, 1, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (5, N'मिक्वाखोला', 1, 1, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (6, N'मेरिङदेन ', 1, 1, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (7, N'मौवाखोला', 1, 1, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (8, N'याङवरक ', 1, 1, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (9, N'रिरिजंगा', 1, 1, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (10, N'फिदिम', 1, 2, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (11, N'फालेलुङ ', 1, 2, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (12, N'फाल्गुनन्द', 1, 2, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (13, N'हिलिहाङ ', 1, 2, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (14, N'कुम्मायक', 1, 2, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (15, N'गोरखा', 4, 37, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (16, N'पालुङटार', 4, 37, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (17, N'आरूघाट', 4, 37, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (18, N'गण्डकी', 4, 37, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (19, N'चुमनुब्री', 4, 37, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (20, N'धार्चे', 4, 37, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (21, N'भिमसेन', 4, 37, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (22, N'सहिद लखन', 4, 37, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (23, N'सिरानचोक', 4, 37, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (24, N'सुलिकोट', 4, 37, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (25, N'अजिरकोट', 4, 37, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (26, N'बेसिसहर', 4, 1, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (27, N'मध्य नेपाल', 4, 38, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (28, N'राईनास', 4, 38, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (29, N'सुन्दर बजार', 4, 38, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (30, N'क्वहोलासोथार', 4, 38, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (31, N'दुधपोखरी', 4, 38, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (32, N'दोर्दी', 4, 38, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (33, N'मिक्लाजुङ ', 1, 2, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (34, N'मस्र्याङ्दि', 4, 38, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (35, N'तुम्बेवा', 1, 2, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (36, N'भानु', 4, 39, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (37, N'भिमाद', 4, 39, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (38, N'व्यास', 4, 39, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (39, N'याङवरक ', 1, 2, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (40, N'शुक्लागण्डकी', 4, 39, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (41, N'इलाम', 1, 3, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (42, N'आँबुखैरेनी', 4, 39, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (43, N'रिसिङ', 4, 39, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (44, N'धिरिङ', 4, 39, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (46, N'देवघाट', 4, 39, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (47, N'म्याग्दे', 4, 39, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (48, N'बन्दीपुर', 4, 39, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (49, N'देउमाई', 1, 3, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (50, N'माई', 1, 3, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (51, N'सुर्योदय', 1, 3, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (52, N'फक्फोकथुम', 1, 3, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (53, N'चुलाचुली', 1, 3, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (54, N'माइजोगमाई', 1, 3, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (55, N'माङसेबुङ ', 1, 3, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (56, N'रोङ ', 1, 3, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (57, N'सन्दकपुर', 1, 3, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (58, N'चैनपुर', 1, 4, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (59, N'धर्मदेवी', 1, 4, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (60, N'खादबारी', 1, 4, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (61, N'मादी', 1, 4, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (62, N'पाँचखपन ', 1, 4, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (63, N'भोटखोला', 1, 4, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (64, N'चिचिला', 1, 4, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (65, N'मकालु', 1, 4, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (66, N'सभापोखरी', 1, 4, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (67, N'सिलीचोङ', 1, 4, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (68, N'म्याङलुङ ', 1, 5, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (69, N'लालिगुराँस', 1, 5, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (70, N'आठराई', 1, 5, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (71, N'छथर', 1, 5, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (72, N'फेदाप', 1, 5, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (73, N'मेन्छयायेम ', 1, 5, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (74, N'पाख्रीबास', 1, 6, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (75, N'धनकुटा', 1, 6, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (76, N'महालक्ष्मी', 1, 6, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (77, N'सागुरीगढी ', 1, 6, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (78, N'खाल्सा छिन्ताङ सहीदभूमि', 1, 6, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (79, N'छथर जोरपाटी', 1, 6, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (80, N'चौबीसे', 1, 6, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (81, N'भोजपुर', 1, 8, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (82, N'षडानन्द', 1, 8, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (83, N'ट्याम्केमैयुम ', 1, 8, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (84, N'अरुण', 1, 8, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (85, N'पौवादुम्मा', 1, 8, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (86, N'रामप्रसाद राई', 1, 8, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (87, N'साल्पासिलिचो', 1, 8, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (88, N'आमाचोक', 1, 8, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (89, N'हतुवागढी ', 1, 8, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (90, N'हलेसी तुवाचुङ', 1, 9, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (91, N'रुपाकोट मझुवागढ़ी', 1, 9, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (92, N'ऐसेलुखर्क ', 1, 9, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (93, N'लामीडाँडा', 1, 9, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (94, N'जन्तेदुंगा', 1, 9, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (95, N'पोखरा लेखनाथ', 4, 40, 1, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (96, N'अन्नपुर्ण', 4, 40, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (97, N'माछापुछ्रे', 4, 40, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (98, N'मादी', 4, 40, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (99, N'रुपा', 4, 40, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (100, N'खोटेहाङ ', 1, 9, 3, 1)
GO
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (101, N'केपीलासगढी', 1, 9, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (102, N'दिप्रुङ ', 1, 9, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (103, N'साकेला ', 1, 9, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (104, N'बराहपोखरी', 1, 9, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (105, N'सोलु दुधकुण्ड', 1, 10, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (106, N'दुधकोशी', 1, 10, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (107, N'खुम्वु पासाङल्हमु', 1, 10, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (108, N'दुधकौशिका', 1, 10, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (109, N'नेचासल्यान ', 1, 10, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (110, N'महाकुलुङ ', 1, 10, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (111, N'लेखु पिके', 1, 10, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (112, N'सोताङ ', 1, 10, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (113, N'सिद्दिचरण', 1, 11, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (114, N'खिजिदेम्वा', 1, 11, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (115, N'चम्पादेवी', 1, 11, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (116, N'चिशंखुगढी ', 1, 11, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (117, N'मानेभञ्ज्याङ', 1, 11, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (118, N'मोलुङ', 1, 11, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (119, N'लिखु ', 1, 11, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (120, N'सुनकोशी', 1, 11, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (121, N'कटारी', 1, 12, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (122, N'चौदण्डीगढी', 1, 12, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (123, N'त्रियुगा', 1, 12, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (124, N'बेलका', 1, 12, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (125, N'उदयपुरगढी ', 1, 12, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (126, N'ताप्ली', 1, 12, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (127, N'रौतामाई ', 1, 12, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (128, N'सुनकोसी', 1, 12, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (129, N'मेची नगर ', 1, 13, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (130, N'दमक', 1, 13, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (131, N'कन्काई', 1, 13, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (132, N'भद्रपुर', 1, 13, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (133, N'चामे', 4, 41, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (134, N'अर्जुनधारा', 1, 13, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (135, N'नासो', 4, 41, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (136, N'शिवसताक्षि ', 1, 13, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (137, N'नेस्याङ', 4, 41, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (138, N'घरपझोङ', 4, 42, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (139, N'गौरादह', 1, 13, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (140, N'थासाङ', 4, 42, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (141, N'दालोमे', 4, 42, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (142, N'लोमन्थाङ', 4, 42, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (143, N'बिर्तामोड', 1, 13, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (144, N'कमल', 1, 13, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (145, N'बार्हगाउँ मुक्तिछ्रेत्र', 4, 42, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (146, N'गौरीगंज', 1, 13, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (147, N'कुस्मा', 4, 43, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (148, N'बाह्रदशी ', 1, 13, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (149, N'फलेवास', 4, 43, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (150, N'जलजला', 4, 43, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (151, N'झापा', 1, 13, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (152, N'पैयुँ', 4, 43, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (153, N'बुद्धशान्ति', 1, 13, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (154, N'महाशीला', 4, 43, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (155, N'हल्दीबारी', 1, 13, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (156, N'मोदी', 4, 43, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (157, N'विहादी', 4, 43, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (158, N'केचनाकवल', 1, 13, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (159, N'बेलबारी', 1, 14, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (160, N'लेटाङ ', 1, 14, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (161, N'पथरी शनिश्चरे', 1, 14, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (162, N'रंगेली', 1, 14, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (163, N'रतुवामाई', 1, 14, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (164, N'सुनवर्षी', 1, 14, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (165, N'उर्लाबारी', 1, 14, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (166, N'सुन्दरहरैंचा', 1, 14, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (167, N'बुढीगंगा', 1, 14, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (168, N'धनपालथान', 1, 14, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (169, N'ग्रामथान', 1, 14, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (170, N'जहदा', 1, 14, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (171, N'कानेपोखरी', 1, 14, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (172, N'कटहरी', 1, 14, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (173, N'केराबारी', 1, 14, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (174, N'मिक्लाजुङ', 1, 14, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (175, N'इनरुवा', 1, 15, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (176, N'दुहबी', 1, 15, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (177, N'रामधुनी', 1, 15, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (178, N'बराह', 1, 15, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (179, N'देवानगन्ज', 1, 15, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (180, N'कोशी', 1, 15, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (181, N'गढी ', 1, 15, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (182, N'बर्जु', 1, 15, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (183, N'भोक्राहा', 1, 15, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (184, N'हरिनगर', 1, 15, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (185, N'गल्याङ ', 4, 44, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (186, N'चापाकोट ', 4, 44, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (187, N'पुतलीबजार ', 4, 44, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (188, N'भीरकोट ', 4, 44, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (189, N'बालिंङ', 4, 44, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (190, N'अर्जुनचौपारी ', 4, 44, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (191, N'आँधिखोला ', 4, 44, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (192, N'कालिगण्डकी ', 4, 44, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (193, N'फेदीखोला ', 4, 44, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (194, N'बिरुवा', 4, 44, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (195, N'हरिनास ', 4, 44, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (196, N'बेनी ', 4, 45, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (197, N'अन्नपुर्ण', 4, 45, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (198, N'धौलागिरी', 4, 45, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (199, N'मालिका', 4, 45, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (200, N'रधुगंगा', 4, 45, 3, 1)
GO
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (201, N'गल्कोट ', 4, 46, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (202, N'जैमुनी', 4, 37, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (203, N'ढोरपाटन', 4, 46, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (204, N'बाग्लुङ', 4, 46, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (205, N'कोठेखोला', 4, 46, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (206, N'तमनखोला', 4, 46, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (207, N'ताराखोला', 4, 46, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (208, N'निसिखोला ', 4, 46, 1, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (209, N'बडीगाड', 4, 46, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (210, N'बरेङ', 4, 46, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (211, N'कावासोती ', 4, 47, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (212, N'गैडाकोट', 4, 47, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (213, N'देबचुली', 4, 47, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (214, N'बर्दघाट', 4, 1, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (216, N'सप्तकोशी कन्चनरुप', 2, 16, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (217, N'हनुमाननगर कङ्‌कालिनी ', 2, 16, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (218, N'राजविराज', 2, 16, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (219, N'मध्यबिन्दु', 4, 47, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (220, N'शम्भुनाथ', 2, 16, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (222, N'बरसाइन', 2, 16, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (223, N'सुनबल', 4, 47, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (229, N'बेल्ही चपेना', 2, 16, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (231, N'छिन्नमस्ता', 2, 16, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (232, N'गोलबजार', 2, 17, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (233, N'धनगढी', 2, 17, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (234, N'मिर्चैया', 2, 17, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (236, N'लहान', 2, 17, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (237, N'सिराहा', 2, 17, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (238, N'सुखीपुर', 2, 17, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (239, N'औरही', 2, 17, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (240, N'कर्जन्हा', 2, 17, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (241, N'कल्याणपुर जब्दी', 2, 17, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (242, N'बिष्णुपुर प्रम', 2, 17, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (243, N'भगवानपुर ', 2, 17, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (244, N'महेशपुर पतारी', 2, 17, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (245, N'बुङदिकाली', 4, 47, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (246, N'गणेशमान चारनाथ', 2, 18, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (247, N'धनुषाधाम', 2, 18, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (249, N'शहीदनगर नगरपालिका', 2, 18, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (251, N'क्षिरेश्वोरनाथ', 2, 18, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (252, N'मिथिला', 2, 18, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (253, N'नगराइन', 2, 18, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (255, N'मिथिला बिहारी', 2, 18, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (256, N'हंसपुर नगरपालिका', 2, 18, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (257, N'कमला सिद्दीदात्री', 2, 18, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (258, N'बर्दिबास', 2, 19, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (259, N'जलेश्वोर', 2, 19, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (260, N'गौशाला', 2, 19, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (261, N'सोनमा गाउँपालिका ', 2, 19, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (262, N'साम्सी गाउँपालिका', 2, 19, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (263, N'लोहरपट्टि', 2, 19, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (264, N'रामगोपालपुर', 2, 19, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (265, N'मनरा शिसवा नगरपालिका ', 2, 19, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (266, N'मटिहानी', 2, 19, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (267, N'भंगाहा', 2, 19, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (268, N'बलवा', 2, 19, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (269, N'औरही', 2, 19, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (270, N'पिपरा', 2, 19, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (271, N'एकडारा', 2, 19, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (272, N'हरिपुर्वा', 2, 20, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (273, N'पर्सा गाउँपालिका', 2, 20, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (274, N'बरहथवा', 2, 20, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (275, N'बलरा', 2, 20, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (276, N'हरिपुर', 2, 20, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (277, N'लालबन्दी', 2, 20, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (278, N'मलंगवा', 2, 20, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (279, N'हरिवन', 2, 20, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (280, N'कविलासी', 2, 20, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (281, N'कौडेना गाउँपालिका', 2, 20, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (282, N'धनकौल पश्चिम', 2, 20, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (283, N'चन्द्रनगर', 2, 20, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (284, N'विष्णु गाउँपालिका', 2, 20, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (285, N'बेल्ही', 2, 20, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (286, N'गोडैटा नगरपालिका ', 2, 20, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (287, N'रामनगर बहुअर्वा', 2, 20, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (288, N'चंद्रपुर', 2, 21, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (289, N'गौर', 2, 21, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (290, N'गरुडा', 2, 21, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (291, N'यमुनामाई गाउँपालिक', 2, 21, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (292, N'दुर्गा भगवती गाउँपालिका', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (293, N'बौधीमाई नगरपालिका', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (294, N'बिजयपुर', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (295, N'परोहा', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (296, N'गढीमाई', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (297, N'गुजरा नगरपालिका', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (298, N'कटहरिया', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (299, N'ईशनाथ', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (300, N'मौलापुर', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (301, N'बृन्दावन नगरपालिका', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (302, N'राजपुर', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (303, N'कोल्हवी नगरपालिका', 2, 22, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (304, N'कलैया', 2, 22, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (305, N'सिम्रोनागढ़ी', 2, 22, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (306, N'महागढ़िमाई', 2, 22, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (307, N'निजगढ', 2, 22, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (308, N'देवताल गाउँपालिका', 2, 22, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (309, N'सुवर्ण गाउँपालिका', 2, 22, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (310, N'बारागढीगाउँपालिका', 2, 22, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (311, N'फेटा गाउँपालिका', 2, 22, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (312, N'प्रसौनी गाउँपालिका', 2, 22, 3, 1)
GO
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (313, N'आदर्श कोटवाल गाउँपालिका', 2, 22, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (314, N'पचरौता सतमौजा', 2, 22, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (315, N'पोखरिया', 2, 23, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (316, N'जगरनाथपुर गाउँपालिका', 2, 23, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (317, N'पर्टेवा सुगौली', 2, 23, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (318, N'सखुवा प्रसौनी', 2, 23, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (320, N'धोबिनी', 2, 23, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (322, N'पकाहा मैनपुर', 2, 23, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (323, N'बुलिङटार', 4, 47, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (326, N'विनयी', 4, 47, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (328, N'हुप्सेकोट', 4, 47, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (329, N'देबदह', 5, 49, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (330, N'लुम्बिनी सास्कृतिक ', 5, 49, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (331, N'जिरी', 3, 24, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (332, N'भिमेश्वोर', 3, 24, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (333, N'कालिन्चोक', 3, 24, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (334, N'गौरीशंकर', 3, 24, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (335, N'तामाकोसी', 3, 24, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (336, N'मेलुङ', 3, 24, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (337, N'बिगु', 3, 24, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (338, N'बैतेश्वोर', 3, 24, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (339, N'सैलुङ', 3, 24, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (340, N'मन्थली', 3, 25, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (341, N'रामेछाप', 3, 25, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (342, N'उमाकुण्ड', 3, 25, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (344, N'सैनामैना ', 5, 49, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (345, N'सिद्धार्थनगर नगरपालिका', 5, 49, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (346, N'तिलोत्तमा ', 5, 49, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (347, N'गैडहवा', 5, 49, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (348, N'कन्चन', 5, 49, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (349, N'कोटहीमाई ', 5, 49, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (350, N'मर्चबारी', 5, 49, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (351, N'मायादेवी', 5, 49, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (353, N'रोहिणी', 5, 49, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (354, N'सम्मारीमाई', 5, 49, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (355, N'सियारी', 5, 49, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (356, N'शुद्दोधन', 5, 49, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (357, N'कपिलवस्तु', 5, 50, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (358, N'बुद्दभुमी', 5, 50, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (359, N'गोकुलगंगा', 3, 25, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (360, N'दोरम्बा', 3, 25, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (361, N'लिखु', 3, 25, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (362, N'सुनापति', 3, 25, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (363, N'खाँडादेवी', 3, 25, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (364, N'कमलामाई', 3, 26, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (365, N'दुधौली', 3, 26, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (366, N'गोलान्जर', 3, 26, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (367, N'घ्याङलेख', 3, 26, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (368, N'तीनपाटन', 3, 26, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (369, N'फिक्कल', 3, 26, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (370, N'मरिण', 3, 26, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (371, N'सुनकोसी', 3, 26, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (372, N'हरिहरपुरगढी', 3, 26, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (373, N'धुलिखेल', 3, 27, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (374, N'बनेपा', 3, 27, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (375, N'पनौती', 3, 27, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (376, N'पाँचखाल', 3, 27, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (377, N'नमोबुद्ध', 3, 27, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (378, N'मण्डन', 3, 27, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (379, N'खानीखोला', 3, 27, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (380, N'चौंरीदेउराली', 3, 27, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (381, N'तेमाल', 3, 27, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (382, N'बेथानचोक', 3, 27, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (383, N'भुम्लु', 3, 27, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (384, N'महाभारत', 3, 27, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (385, N'रोसी', 3, 27, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (386, N'शिवराज', 5, 50, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (387, N'महाराजगन्ज', 5, 50, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (388, N'चौतारा सागाचोकगढ़ी', 3, 28, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (389, N'बाह्रबिसे', 3, 28, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (390, N'मेलम्ची', 3, 28, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (391, N'इन्द्राबती', 3, 28, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (392, N'जुगल', 3, 28, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (393, N'पाँचपोखरी थाङपाल', 3, 28, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (394, N'बलेफी', 3, 28, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (395, N'भोटेकोसी', 3, 28, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (396, N'लिसंखु पाखर', 3, 28, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (397, N'सुनकोसी', 3, 28, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (398, N'कृष्णनगर', 5, 50, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (399, N'हेलम्बु', 3, 28, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (400, N'त्रिपुरासुन्दरी', 3, 28, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (401, N'बाणगंगा', 5, 50, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (402, N'मायादेवी', 5, 50, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (403, N'उत्तरगया', 3, 29, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (404, N'कालिका', 3, 29, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (405, N'गोसाईकुण्ड', 3, 29, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (406, N'नौकुण्ड', 3, 29, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (407, N'यशोधरा', 5, 50, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (408, N'पार्वतीकुण्ड', 3, 29, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (409, N'शुद्दोधन', 5, 50, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (410, N'विजयनगर', 5, 50, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (411, N'विदुर', 3, 30, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (412, N'ककनी', 3, 30, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (413, N'किस्पाङ', 3, 30, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (414, N'रामपुर', 5, 51, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (415, N'तानसेन', 5, 51, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (416, N'कोल्पू', 3, 30, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (417, N'निस्दी', 5, 51, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (418, N'तादी', 3, 30, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (419, N'पुर्वखोला', 5, 51, 3, 1)
GO
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (420, N'तारकेश्वोर', 3, 30, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (421, N'दुप्चेश्वोर', 3, 30, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (422, N'पञ्चकन्या', 3, 30, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (424, N'बेलकोटगढी', 3, 30, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (425, N'मेघाङ', 3, 30, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (426, N'शिवपुरी', 3, 30, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (427, N'रम्भा', 5, 51, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (428, N'सुर्यगढी', 3, 30, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (429, N'माथागढ़ी', 5, 51, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (430, N'तिनाउ', 5, 51, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (431, N'बगनासकाली', 5, 51, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (432, N'रिब्दिकोट', 5, 51, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (433, N'रैनादेबी छहरा', 5, 51, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (434, N'चाँगुनारायण', 3, 33, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (435, N'भक्तपुर', 3, 33, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (436, N'मध्यपुर थिमी', 3, 33, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (437, N'सुर्यविनायक', 3, 33, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (438, N'कालिका', 3, 32, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (439, N'खैरहनी', 3, 32, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (440, N'माडी', 3, 32, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (441, N'रत्ननगर', 3, 32, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (442, N'राप्ती', 3, 32, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (443, N'इच्छाकामना', 3, 32, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (444, N'थाहा', 3, 34, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (445, N'इन्द्रसरोवर', 3, 34, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (446, N'कैलाश', 3, 34, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (447, N'बकैया', 3, 34, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (448, N'बागमती', 3, 34, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (449, N'भीमफेदी', 3, 34, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (450, N'मकवानपुरगढी गाउँपालिका', 3, 34, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (451, N'मनहरी', 3, 34, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (452, N'रक्सीराङ', 3, 34, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (453, N'गोदावरी', 3, 35, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (454, N'महालक्ष्मी', 3, 35, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (456, N'कोन्ज्योसोम', 3, 35, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (457, N'बागमती', 3, 35, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (458, N'महाङ्काल', 3, 35, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (459, N'काठमाडौँ', 3, 36, 1, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (460, N'कागेश्वोरी मनोहरा', 3, 36, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (461, N'किर्तिपुर', 3, 36, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (462, N'गोकर्णेश्वोर', 3, 36, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (463, N'चन्द्रागिरी', 3, 36, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (464, N'टोखा', 3, 36, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (465, N'तारकेश्वोर', 3, 36, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (466, N'नागार्जुन', 3, 36, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (467, N'बूढानीलकण्ठ', 3, 36, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (468, N'धुनीबेंसी', 3, 31, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (469, N'नीलकण्ठ', 3, 31, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (470, N'खनियाबास', 3, 31, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (471, N'गजुरी', 3, 31, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (472, N'गल्छी', 3, 31, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (473, N'गंगाजमुना', 3, 31, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (474, N'ज्वालामुखी', 3, 31, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (475, N'थाक्रे', 3, 31, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (476, N'नेत्रावती', 3, 31, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (477, N'बेनीघाट रोराङ्ग', 3, 31, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (478, N'रुवी भ्याली', 3, 31, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (479, N'सिद्धलेक', 3, 31, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (480, N'त्रिपुरासुन्दरी', 3, 31, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (481, N'आठबिसकोट', 6, 60, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (482, N'चौरजहारी', 6, 60, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (483, N'मुसिकोट', 6, 60, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (484, N'त्रिवेणी', 6, 60, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (486, N'बाँफिकोट', 6, 60, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (488, N'सानीभेरी', 6, 60, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (490, N'सन्धिखर्क', 5, 52, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (491, N'शितगंगा', 5, 52, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (492, N'भुमिकास्थान', 5, 52, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (493, N'शारदा', 6, 61, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (494, N'बागचौर', 6, 61, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (495, N'बनगाड कुपिण्डे', 6, 61, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (496, N'सरावल गाउँपालिका', 5, 48, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (500, N'कालीमाटी', 6, 61, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (501, N'त्रिवेणी', 6, 61, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (502, N'कपुरकोट', 6, 61, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (503, N'छत्रेश्वरी', 6, 61, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (504, N'पाणीनी', 5, 52, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (505, N'मालारानी', 5, 52, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (506, N'ढोरचौर', 6, 61, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (507, N'मुसिकोट', 5, 53, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (508, N'कुमाखमालिका', 6, 61, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (509, N'दार्मा', 6, 61, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (510, N'रेसुङ्गा', 5, 53, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (511, N'ठूली भेरी ', 6, 62, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (512, N'त्रिपुरासुन्दरी', 6, 62, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (513, N'डोल्पो बुद्ध', 6, 62, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (514, N'से फोक्सुन्डो', 6, 62, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (515, N'कालिगण्डकी ', 5, 53, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (516, N'जगदुल्ला', 6, 62, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (517, N'मुड्केचुला', 6, 62, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (518, N'गुल्मिदरबार', 5, 53, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (519, N'काईके', 6, 62, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (520, N'छार्का ताङसोङ', 6, 62, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (521, N'चन्दननाथ', 6, 63, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (522, N'कनकासुन्दरी', 6, 63, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (523, N'सिंजा', 6, 63, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (524, N'हिमा', 6, 63, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (525, N'तिला', 6, 63, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (526, N'गुठीचौर', 6, 63, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (527, N'तातोपानी', 6, 63, 3, 1)
GO
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (528, N'पातारासी', 6, 63, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (529, N'छायानाथ रारा', 6, 64, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (530, N'चन्द्रकोट', 5, 53, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (531, N'मुगुम कार्मारोंग', 6, 64, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (532, N'सत्यवती', 5, 53, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (533, N'सोरु', 6, 64, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (534, N'रुरु', 5, 53, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (535, N'खत्याड', 6, 64, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (536, N'सिमकोट', 6, 65, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (537, N'नाम्खा', 6, 65, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (538, N'खार्पुनाथ', 6, 65, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (539, N'सर्केगाड', 6, 65, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (540, N'चंखेली', 6, 65, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (541, N'अदानचुली', 6, 65, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (542, N'ताँजाकोट', 6, 65, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (543, N'धुर्कोट', 5, 53, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (544, N'मदाने', 5, 53, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (545, N'मालिका', 5, 53, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (546, N'खाँडाचक्र', 6, 66, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (547, N'रास्कोट', 6, 66, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (548, N'तिलागुफा', 6, 66, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (549, N'पचालझरना', 6, 66, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (550, N'सान्नी', 6, 66, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (551, N'नरहरिनाथ', 6, 66, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (552, N'इस्मा', 5, 53, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (553, N'कालिका', 6, 66, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (554, N'महावै', 6, 66, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (556, N'पलाता', 6, 66, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (557, N'भेरी', 6, 67, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (559, N'छेडागाड', 6, 67, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (560, N'त्रिवेणी नलगाड', 6, 67, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (561, N'बारेकोट', 6, 67, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (562, N'कुसे', 6, 67, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (564, N'जुनीचाँदे', 6, 67, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (565, N'शिवालय', 6, 67, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (566, N'नारायण', 6, 68, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (567, N'दुल्लु', 6, 68, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (568, N'चामुण्डा विन्द्रासैनी', 6, 68, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (569, N'आठबीस', 6, 68, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (570, N'भगवतीमाई', 6, 68, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (571, N'गुराँस', 6, 68, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (572, N'डुंगेश्वर', 6, 68, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (573, N'नौमुले', 6, 68, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (574, N'महावु', 6, 68, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (575, N'भैरवी', 6, 68, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (576, N'ठाँटीकाँध', 6, 68, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (577, N'विरेन्द्रनगर', 6, 69, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (578, N'भेरीगंगा', 6, 69, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (579, N'गुर्भाकोट', 6, 69, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (580, N'पञ्चपुरी', 6, 69, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (581, N'लेकबेशी', 6, 69, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (582, N'चौकुने', 6, 69, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (583, N'बराहताल', 6, 69, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (584, N'चिङ्गाड', 6, 69, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (585, N'सिम्ता', 6, 69, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (586, N'बडीमालिका', 7, 70, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (587, N'त्रिवेणी', 7, 70, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (588, N'बुढीगंगा', 7, 70, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (589, N'बुढीनन्दा', 7, 70, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (590, N'गौमुल', 7, 70, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (591, N'पाण्डव गुफा', 7, 70, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (592, N'स्वामीकार्तिक', 7, 70, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (593, N'छेडेदह', 7, 70, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (594, N'हिमाली', 7, 70, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (595, N'जयपृथ्वी', 7, 71, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (596, N'पुथा उत्तरगंगा गाउँपालिका', 5, 54, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (597, N'बुंगल', 7, 71, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (598, N'केदारस्युँ ', 7, 71, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (599, N'खप्तडछान्ना', 7, 71, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (600, N'छबिसपाथिभेरा', 7, 71, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (601, N'तलकोट', 7, 71, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (602, N'थलारा', 7, 71, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (603, N'दुर्गाथली', 7, 71, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (604, N'भूमे गाउँपालिका', 5, 54, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (605, N'मष्टा', 7, 71, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (606, N'सिस्ने', 5, 54, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (607, N'वित्थडचिर', 7, 71, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (608, N'सूर्मा', 7, 71, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (610, N'दिपायल सिलगढी', 7, 72, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (611, N'शिखर', 7, 72, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (612, N'पूर्वीचौकी', 7, 72, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (613, N'बडीकेदार', 7, 72, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (614, N'जोरायल', 7, 72, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (617, N'सायल', 7, 72, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (618, N'आदर्श', 7, 72, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (619, N'के.आई.सिं.', 7, 72, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (620, N'रोल्पा', 5, 55, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (621, N'बोगटान', 7, 72, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (622, N'त्रिवेणी', 5, 55, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (623, N'दुइखोला', 5, 55, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (624, N'मंगलसेन', 7, 73, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (625, N'कमलबजार', 7, 73, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (626, N'साफेबगर', 7, 73, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (627, N'माढी', 5, 55, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (628, N'माढी', 5, 55, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (629, N'पञ्चदेवल विनायक', 7, 73, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (630, N'चौरपाटी', 7, 73, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (631, N'मेल्लेख', 7, 73, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (632, N'बान्नीगढी जयगढ', 7, 73, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (633, N'रामारोशन', 7, 73, 3, 1)
GO
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (634, N'ढकारी', 7, 73, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (635, N'तुर्माखाँद', 7, 73, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (636, N'रुन्टिगढी', 5, 55, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (637, N'लुंगी', 5, 55, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (638, N'महाकाली', 7, 74, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (639, N'सुकिदह', 5, 55, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (640, N'शैल्यशिखर', 7, 74, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (641, N'सुनछहरी', 5, 55, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (642, N'मालिकार्जुन', 7, 74, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (643, N'सुवर्णबती', 5, 55, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (644, N'अपिहिमाल', 7, 74, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (645, N'दुहुँ', 7, 74, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (646, N'नौगाड', 7, 74, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (647, N'मार्मा', 7, 74, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (648, N'लेकम', 7, 74, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (649, N'थवाङ', 5, 55, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (650, N'ब्यास', 7, 74, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (651, N'दशरथचन्द', 7, 75, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (652, N'पाटन', 7, 75, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (653, N'प्युठान', 5, 56, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (654, N'स्वर्गद्दारी', 5, 56, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (655, N'मेलौली', 7, 75, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (656, N'पुर्चौडी', 7, 75, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (657, N'गौमुखी', 5, 56, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (658, N'सुर्नया', 7, 75, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (659, N'सिगास', 7, 75, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (660, N'शिवनाथ', 7, 75, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (661, N'माण्डवी', 5, 56, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (662, N'पन्चेश्वर', 7, 75, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (663, N'दोगडाकेदार', 7, 75, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (664, N'सरुमारानी', 5, 56, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (665, N'डीलासैनी', 7, 75, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (666, N'मल्लरानी', 5, 56, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (667, N'अमरगढी', 7, 76, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (668, N'परशुराम', 7, 76, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (669, N'नौबहिनी', 5, 56, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (670, N'आलिताल', 7, 76, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (671, N'भागेश्वर', 7, 76, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (672, N'झिमरुक्क', 5, 56, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (673, N'नवदुर्गा', 7, 76, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (674, N'अजयमेरु', 7, 76, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (675, N'गन्यापधुरा', 7, 76, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (676, N'ऐराबती', 5, 1, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (677, N'तुल्सीपुर', 5, 57, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (678, N'घोराही ', 5, 57, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (679, N'लमही', 5, 57, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (680, N'बंगलाचुली', 5, 57, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (681, N'दगीशरण', 5, 57, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (682, N'गढवा', 5, 57, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (683, N'बिराटनगर', 1, 14, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (684, N'इटहरी', 1, 15, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (685, N'धरान', 1, 15, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (686, N'भिमदत्त', 7, 77, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (687, N'पुनर्वास', 7, 77, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (688, N'वेदकोट', 7, 77, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (689, N'माहाकाली', 7, 77, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (690, N'सुक्लाफाँटा', 7, 77, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (691, N'बेलौरी', 7, 77, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (692, N'कृष्णपुर', 7, 77, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (693, N'बेलडाडी', 7, 77, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (694, N'लालझाँडी', 7, 77, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (695, N'धनगढी', 7, 78, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (696, N'टिकापुर', 7, 78, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (697, N'घोडाघोडी', 7, 78, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (698, N'लम्कीचुहा', 7, 78, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (699, N'भजनी', 7, 78, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (700, N'गोदावरी', 7, 78, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (701, N'गौरीगंगा', 7, 78, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (702, N'जानकी', 7, 78, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (703, N'बर्दगोरीया', 7, 78, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (704, N'मोहन्याल', 7, 78, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (705, N'कैलारी', 7, 78, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (706, N'जोशीपुर', 7, 78, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (707, N'चुरे', 7, 78, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (708, N'राजपुर', 5, 57, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (709, N'जनकपुर', 2, 18, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (710, N'सिमरा जितपुर', 2, 22, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (711, N'बिरगंज महानगरपालिका', 2, 23, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (712, N'राप्ती', 5, 57, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (713, N'शान्तिनगर ', 5, 57, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (714, N'बबई', 5, 57, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (715, N'नेपालगन्ज', 5, 58, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (716, N'कोहलपुर', 5, 58, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (717, N'नरैनापुर', 5, 58, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (718, N'राप्तिसोनारी', 5, 58, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (719, N'भरतपुर', 3, 32, 1, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (720, N'हेटौडा उपमहानगरपालिका', 3, 34, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (721, N'बैजनाथ', 5, 58, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (722, N'ललितपुर', 3, 35, 1, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (723, N'खजुरा', 5, 58, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (724, N'डुडुवा', 5, 58, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (725, N'जानकी', 5, 58, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (726, N'गुलरिया', 5, 59, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (727, N'मधुबन', 5, 59, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (728, N'राजापुर', 5, 59, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (729, N'ठाकुरबाबा', 5, 59, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (730, N'बासगढी', 5, 59, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (731, N'बारबर्दिया', 5, 59, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (732, N'गेरुवा', 5, 59, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (733, N'बढैयाताल ', 5, 59, 3, 1)
GO
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (734, N'दक्षिणकाली नगरपालिका', 3, 36, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (735, N'शंखरापुर', 3, 36, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (736, N'कन्चनपुर', 2, 16, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (737, N'डाक्नेश्वरी नगरपालिका', 2, 16, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (738, N'बोदेबरसाईन नगरपालिका', 2, 16, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (739, N'सुरुङ्‍गा नगरपालिका', 2, 16, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (740, N'अग्निसाइर कृष्णासवरन गाउँपालिका', 2, 16, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (741, N'महादेवा गाउँपालिका', 2, 16, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (742, N'तिरहुत गाउँपालिका', 2, 16, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (743, N'तिलाठी कोईलाडी गाउँपालिका', 2, 16, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (744, N'रुपनी गाउँपालिका', 2, 16, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (745, N'बिष्णुपुर गाउँपालिका', 2, 16, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (746, N'बलान-बिहुल गाउँपालिका', 2, 16, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (747, N'बरियारपट्टी गाउँपालिका ', 2, 17, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (748, N'लक्ष्मीपुर पतारी गाउँपालिका ', 2, 17, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (750, N'नरहा गाउँपालिका ', 2, 17, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (751, N'सखुवानान्कारकट्टी गाउँपालिका', 2, 17, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (752, N'अर्नमा गाउँपालिका', 2, 17, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (753, N'नवराजपुर गाउँपालिका', 2, 17, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (754, N'विदेह नगरपालिका', 2, 18, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (755, N'सबैला नगरपालिका', 2, 18, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (756, N'जनकनन्दिनी गाउँपालिका', 2, 18, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (757, N' बटेश्वर गाउँपालिका ', 2, 18, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (758, N'मुखियापट्टी मुसहरमिया गाउँपालिका ', 2, 18, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (759, N'लक्ष्मीनिया गाउँपालिका ', 2, 18, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (760, N'औरही गाउँपालिका', 2, 18, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (761, N'धनौजी गाउँपालिका', 2, 18, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (762, N'महोत्तरी गाउँपालिका ', 2, 19, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (763, N'ईश्वरपुर नगरपालिका ', 2, 20, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (764, N'बागमती नगरपालिका', 2, 20, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (765, N'चक्रघट्टा गाउँपालिका ', 2, 20, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (766, N'ब्रह्मपुरी गाउँपालिका ', 2, 20, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (767, N'देवाही गोनाही नगरपालिका', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (768, N'कटहरिया नगरपालिका', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (769, N'माधव नारायण नगरपालिका', 2, 21, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (770, N'करैयामाई गाउँपालिका', 2, 22, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (771, N'परवानीपुर गाउँपालिका', 2, 22, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (772, N'विश्रामपुर गाउँपालिका', 2, 22, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (773, N'बहुदरमाई नगरपालिका', 2, 23, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (774, N'पर्सागढी नगरपालिका', 2, 23, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (775, N'ठोरी गाउँपालिका', 2, 23, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (776, N'छिपहरमाई गाउँपालिका', 2, 23, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (777, N'बिन्दबासिनी गाउँपालिका', 2, 23, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (778, N'कालिकामाई गाउँपालिका', 2, 23, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (779, N'जिरा भवानी गाउँपालिका', 2, 23, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (780, N'बेसीशहर नगरपालिका', 4, 38, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (781, N'नारफू गाउँपालिका', 4, 41, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (782, N'मंगला गाउँपालिका', 4, 45, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (783, N'जैमूनी नगरपालिका', 4, 46, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (784, N'बर्दघाट नगरपालिका', 5, 48, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (785, N'रामग्राम नगरपालिका', 5, 48, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (786, N'सुनवल नगरपालिका', 5, 48, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (788, N'सुस्ता गाउँपालिका', 5, 48, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (789, N'पाल्हीनन्दन गाउँपालिका', 5, 48, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (790, N'प्रतापपुर गाउँपालिका', 5, 48, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (791, N'बुटवल उपमहानगरपालिका', 5, 49, 2, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (792, N'ओमसतिया गाउँपालिका', 5, 49, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (793, N'छत्रदेव गाउँपालिका', 5, 52, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (794, N'छत्रकोट गाउँपालिका', 5, 53, 4, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (795, N'ऐरावती गाउँपालिका', 5, 56, 3, 1)
INSERT [dbo].[provincesVDCDetails] ([ProvincesVDCDetailsID], [VDCName], [ProvincesId], [ProvincesDistrictId], [VdcOrMunicipilityTypeId], [Status]) VALUES (796, N'काँडा गाउँपालिका', 7, 71, 4, 1)
SET IDENTITY_INSERT [dbo].[provincesVDCDetails] OFF
SET IDENTITY_INSERT [dbo].[UserProfile] ON 

INSERT [dbo].[UserProfile] ([UserId], [UserName], [UserType], [UserGroup], [EmployeeId]) VALUES (1, N'admin', 1, 1, 1)
SET IDENTITY_INSERT [dbo].[UserProfile] OFF
SET IDENTITY_INSERT [dbo].[UserType] ON 

INSERT [dbo].[UserType] ([UserTypeId], [UserTypeName], [IsApproved], [CreatedDate], [CreatedBy], [ModifiedDate]) VALUES (1, N'Mangement Level', 0, CAST(N'2017-12-08' AS Date), 1, CAST(N'2017-12-08' AS Date))
SET IDENTITY_INSERT [dbo].[UserType] OFF
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (1, CAST(N'2017-12-07 06:08:09.663' AS DateTime), NULL, 1, CAST(N'2017-12-11 06:37:41.813' AS DateTime), 0, N'AJxm0Nf7ojF1vazQhrsUmaJ05t1YBbiRZz9eK1QPzR6fiP/A431iZYcxjxeIaDOcHw==', CAST(N'2017-12-07 06:08:09.663' AS DateTime), N'', NULL, NULL)
INSERT [dbo].[webpages_Membership] ([UserId], [CreateDate], [ConfirmationToken], [IsConfirmed], [LastPasswordFailureDate], [PasswordFailuresSinceLastSuccess], [Password], [PasswordChangedDate], [PasswordSalt], [PasswordVerificationToken], [PasswordVerificationTokenExpirationDate]) VALUES (2, CAST(N'2017-12-11 10:39:02.403' AS DateTime), NULL, 1, NULL, 0, N'AHr6js6UVLSc769sPo4c1yW1zcEof4fvau7e04iwjr6jXbnm2Eyx9JCv+hBI6Am26g==', CAST(N'2017-12-11 10:39:02.403' AS DateTime), N'', NULL, NULL)
SET IDENTITY_INSERT [dbo].[webpages_Roles] ON 

INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (4, N'Agency')
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (1, N'Primary Actor')
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (2, N'Stackholder')
INSERT [dbo].[webpages_Roles] ([RoleId], [RoleName]) VALUES (3, N'Supporting Actor')
SET IDENTITY_INSERT [dbo].[webpages_Roles] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__UserProf__C9F28456682B27DB]    Script Date: 12/12/2017 1:58:41 PM ******/
ALTER TABLE [dbo].[UserProfile] ADD UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__webpages__8A2B6160B5D57BDE]    Script Date: 12/12/2017 1:58:41 PM ******/
ALTER TABLE [dbo].[webpages_Roles] ADD UNIQUE NONCLUSTERED 
(
	[RoleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[webpages_Roles] ([RoleId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_RoleId]
GO
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserProfile] ([UserId])
GO
ALTER TABLE [dbo].[webpages_UsersInRoles] CHECK CONSTRAINT [fk_UserId]
GO
/****** Object:  StoredProcedure [dbo].[SP_MAIN_USERACTION]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SP_MAIN_USERACTION]
AS
BEGIN
select AclControllerRecordId,AclControllerLabel from AclControllerRecords 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_USERACTION]    Script Date: 12/12/2017 1:58:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_USERACTION]
AS
BEGIN
select a.AclActionRecordId,a.AclActionLabel,c.AclControllerLabel,c.AclControllerRecordId from AclControllerRecords c
inner join AclActionRecords a on c.AclControllerRecordId=a.AclControllerRecordId
END
GO
USE [master]
GO
ALTER DATABASE [PersoanlSecurity] SET  READ_WRITE 
GO
