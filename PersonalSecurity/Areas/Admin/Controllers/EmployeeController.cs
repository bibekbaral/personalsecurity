﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PersonalSecurity.Areas.Admin.Models;
using PersonalSecurity.Areas.Admin.Providers;

namespace PersonalSecurity.Areas.Admin.Controllers
{
    public class EmployeeController : Controller
    {
        //
        // GET: /Admin/Employee/
        EmployeeRecordsProviders pro = new EmployeeRecordsProviders();
        public ActionResult Index()
        {
            EmployeeRecordModel model = new EmployeeRecordModel();
            model.EmployeeRecordModelList = pro.GetList();
            return View(model);
        }

        public ActionResult Create()
        {
            EmployeeRecordModel model = new EmployeeRecordModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(EmployeeRecordModel model)
        {
            if (ModelState.IsValid)
            {
                if (pro.Insert(model))
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(model);
                }
            }
            {
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            EmployeeRecordModel model = new EmployeeRecordModel();
            model = pro.GetList().SingleOrDefault(x => x.EmployeeId == id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(EmployeeRecordModel model)
        {
            pro.Edit(model);
            return RedirectToAction("Index");
        }
    }
}
