﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PersonalSecurity.Areas.Admin.Models;
using PersonalSecurity.Areas.Admin.Providers;

namespace PersonalSecurity.Areas.Admin.Controllers
{
    public class UserTypeSetupController : Controller
    {
        //
        // GET: /Admin/UserTypeSetup/
        UserTypeSetupProviders pro = new UserTypeSetupProviders();
        public ActionResult Index()
        {
            UserTypeModel model = new UserTypeModel();
            model.UserTypeList = pro.GetUserTypeList();
            return View(model);
        }
        public ActionResult Create()
        {
            UserTypeModel model = new UserTypeModel();
            return View(model);

        }
        [HttpPost]
        public ActionResult Create(UserTypeModel model)
        {
            if (ModelState.IsValid)
            {
                if (pro.InsertUserType(model))
                {
                    return RedirectToAction("Index");

                }
                else
                {
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            UserTypeModel model = new UserTypeModel();
            model = pro.GetUserTypeList().FirstOrDefault(x => x.UserTypeId == id);
            return View(model);

        }

        [HttpPost]
        public ActionResult Edit(UserTypeModel model)
        {
            if (pro.EditUserType(model.UserTypeId, model))
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }
        public ActionResult Delete(int id)
        {
            if (pro.DeleteUserType(id))
            {
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

    }
}
