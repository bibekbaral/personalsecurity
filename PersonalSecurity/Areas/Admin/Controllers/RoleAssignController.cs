﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PersonalSecurity.Areas.Admin.Models;

namespace PersonalSecurity.Areas.Admin.Controllers
{
    public class RoleAssignController : Controller
    {
        //
        // GET: /Admin/RoleAssign/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddUserRoleRights()
        {
            UserRoleRightsModel model = new UserRoleRightsModel();
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                model.UserRoleRightsModelList = ent.Database.SqlQuery<UserRoleRightsModel>("SP_USERACTION").OrderBy(x => x.AclActionRecordId).ToList();
                model.UserRoleRightsModelMainList = ent.Database.SqlQuery<UserRoleRightsModel>("SP_MAIN_USERACTION").ToList();

                return View(model);
            }

        }

    }
}
