﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PersonalSecurity.Areas.Admin.Models;
using PersonalSecurity.Areas.Admin.Providers;

namespace PersonalSecurity.Areas.Admin.Controllers
{
    public class AgencySetupController : Controller
    {
        //
        // GET: /Admin/AgencySetup/
        AgencySetupProviders pro = new AgencySetupProviders();
        public ActionResult Index()
        {
            AgencySetupModel model = new AgencySetupModel();
            model.AgencySetupModelList = pro.PopulateAgencySetupList();
            return View(model);
        }

        public ActionResult Create()
        {
            AgencySetupModel model = new AgencySetupModel();
            model.ProvincesId = 1;
            model.ProvincesDistrictId = 1;
            //Get District Id 
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(AgencySetupModel model)
        {
            model.Status = true;
            //Check If Username Already Exist or not
            if (PersonalSecurity.CommonUtilities.IsUserNameAlreadyExist(model.EmailId))
            {
                //
                ViewBag.ErrorMessage = "निकायको ईमेल/प्रयोगकर्ता को विवरण प्रयोग भैसेको छ ।";
                return View(model);
            }
            else
            {
                try
                {
                    if (pro.insertAgencySetup(model) == "Saved Successfully")
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.ErrorMessage = "Error...Please try again";
                        return View(model);
                    }
                }


                catch (Exception)
                {

                    ViewBag.ErrorMessage = "Error...Please try again";
                    return View(model);
                }
            }
        }

        public ActionResult Edit(int id)
        {
            AgencySetupModel model = new AgencySetupModel();
            model = pro.PopulateAgencySetupList().SingleOrDefault(x => x.AgencyId == id);
            return View(model);
        }


        [HttpPost]
        public ActionResult Edit(AgencySetupModel model)
        {
            if (pro.UpdateAgencySetup(model) == "Updated Successfully")
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            return RedirectToAction("Index");
        }

        public ActionResult RegisterAgency(int id)
        {
            return View();
        }


        public ActionResult AddGeographicCoverage(int id)
        {
            AgencySetupModel model = new AgencySetupModel();
            model.AgencyId = id;
            model.AgencyGeographicCoverageModelList = new List<AgencyGeographicCoverageModel>();
            model.AgencyGeographicCoverageModelList = pro.GetDistrictList();

            //Check if agency already inserted geo details....

            if (pro.IsAgencyAlreadyInsertedGeoDetails(id))
            {
                model.AgencyGeographicCoverageModelList = pro.GetDistrictListForEdit(id);
            }
            return View(model);

        }

        [HttpPost]
        public ActionResult AddGeographicCoverage(AgencySetupModel model)
        {
            pro.InsertGeoCoverageOfAgency(model);
            return RedirectToAction("Index");

        }


    }
}
