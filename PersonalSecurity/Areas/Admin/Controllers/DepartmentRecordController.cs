﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PersonalSecurity.Areas.Admin.Models;
using PersonalSecurity.Areas.Admin.Providers;

namespace PersonalSecurity.Areas.Admin.Controllers
{
    [Authorize]
    public class DepartmentRecordController : Controller
    {
        //
        // GET: /Admin/DepartmentRecord/
        DepartmentRecordsProviders pro = new DepartmentRecordsProviders();
        public ActionResult Index()
        {
            DepartmentRecordsModel model = new DepartmentRecordsModel();
            model.DepartmentRecordsModelList = pro.GetList();
            return View(model);
        }
        public ActionResult Create()
        {
            DepartmentRecordsModel model = new DepartmentRecordsModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(DepartmentRecordsModel model)
        {
            if (ModelState.IsValid)
            {
                if (pro.Insert(model))
                {
                    return RedirectToAction("Index");
                }
                else
                    return View(model);
            }
            else
                return View(model);
        }
        public ActionResult Edit(int id)
        {
            DepartmentRecordsModel model = new DepartmentRecordsModel();
            model = pro.GetList().SingleOrDefault(x => x.DepartmentRecordsId == id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(DepartmentRecordsModel model)
        {
            if (ModelState.IsValid)
            {
                if (pro.Edit(model))
                {
                    return RedirectToAction("Index");
                }
                else
                    return View(model);
            }
            else
                return View(model);
        }

    }
}
