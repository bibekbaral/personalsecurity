﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PersonalSecurity.Areas.Admin.Providers;
using PersonalSecurity.Areas.Admin.Models;

namespace PersonalSecurity.Areas.Admin.Controllers
{
    [Authorize]
    public class DesignationSetupController : Controller
    {
        //
        // GET: /Admin/DesignationSetup/

        DesignationSetupProviders pro = new DesignationSetupProviders();
        public ActionResult Index()
        {
            DesignationModel model = new DesignationModel();
            model.DesignationList = pro.GetList();
            return View(model);
        }
        public ActionResult Create()
        {
            DesignationModel model = new DesignationModel();
            model.Status = true;
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(DesignationModel model)
        {
            if (ModelState.IsValid)
            {
                if (pro.Insert(model))
                {
                    return RedirectToAction("Index");
                }
                else
                    return View(model);
            }
            else
                return View(model);
        }
        public ActionResult Edit(int id)
        {
            DesignationModel model = new DesignationModel();
            model = pro.GetList().SingleOrDefault(x => x.DesignationSetupId == id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(DesignationModel model)
        {
            if (ModelState.IsValid)
            {
                pro.Edit(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }

        }

        public ActionResult Delete(int id)
        {
            return RedirectToAction("Index");
        }

    }
}
