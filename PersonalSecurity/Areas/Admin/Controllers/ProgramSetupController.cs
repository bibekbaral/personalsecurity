﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PersonalSecurity.Areas.Admin.Models;
using PersonalSecurity.Areas.Admin.Providers;


namespace PersonalSecurity.Areas.Admin.Controllers
{
    public class ProgramSetupController : Controller
    {
        //
        // GET: /Admin/ProgramSetup/

        ProgramSetupProviders pro = new ProgramSetupProviders();
        public ActionResult Index()
        {
            ProgramSetupModel model = new ProgramSetupModel();
            model.ProgramSetupMasterModelList = pro.PopulateProgramList();
            
            return View(model);
        }
        public ActionResult Create()
        {
            ProgramSetupModel model = new ProgramSetupModel();
            model.ObjProgramSetupMasterModel.StartDate = DateTime.Today;
            model.ObjProgramSetupMasterModel.EndDate = DateTime.Today;
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(ProgramSetupModel model)
        {
            pro.InsertProgramSetup(model);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            ProgramSetupModel model = new ProgramSetupModel();
            model.ObjProgramSetupMasterModel = pro.PopulateProgramList().SingleOrDefault(x => x.ProgramSetupId == id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ProgramSetupModel model)
        {
            pro.UpdateProgramSetup(model);
            return RedirectToAction("Index");
        }


        public ActionResult Delete(int id)
        {
            return RedirectToAction("Index");
        }



    }
}
