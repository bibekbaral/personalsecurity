﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PersonalSecurity.Areas.Admin.Models;
using System.Data;

namespace PersonalSecurity.Areas.Admin.Providers
{
    public class DepartmentRecordsProviders
    {
        public List<DepartmentRecordsModel> GetList()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                List<DepartmentRecordsModel> ReturnList = ent.DepartmentRecords.Select(x => new DepartmentRecordsModel()
                {
                    DepartmentNameEng = x.DepartmentNameEng,
                    DepartmentNameNep = x.DepartmentNameNep,
                    DepartmentRecordsId = x.DepartmentRecordsId,
                    Status = x.Status

                }).ToList();

                return ReturnList;
            }
        }
        public bool Insert(DepartmentRecordsModel model)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var InsertObj = new DepartmentRecords
                {
                    DepartmentNameEng=model.DepartmentNameEng,
                    DepartmentNameNep=model.DepartmentNameNep,
                    Status=model.Status
                };
                ent.DepartmentRecords.Add(InsertObj);
                int i = ent.SaveChanges();
                if (i > 0)
                {
                    return true;
                }
                else
                    return false;

            }

        }

        public bool Edit(DepartmentRecordsModel model)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var EditObj = ent.DepartmentRecords.SingleOrDefault(x => x.DepartmentRecordsId == model.DepartmentRecordsId);
                if (EditObj != null)
                {
                    EditObj.DepartmentNameEng = model.DepartmentNameEng;
                    EditObj.DepartmentNameNep = model.DepartmentNameNep;
                    EditObj.Status = model.Status;
                    ent.Entry(EditObj).State = EntityState.Modified;
                    int i = ent.SaveChanges();
                    if (i > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
        }







    }
}