﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PersonalSecurity.Areas.Admin.Models;
using System.Data.SqlClient;
using System.Data;

namespace PersonalSecurity.Areas.Admin.Providers
{

    public class ProgramSetupProviders
    {

        public List<ProgramSetupMasterModel> PopulateProgramList()
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                List<ProgramSetupMasterModel> ProgramList = new List<ProgramSetupMasterModel>();
                ProgramList = db.Database.SqlQuery<ProgramSetupMasterModel>("PopulateProgramSetup").ToList();
                return ProgramList;
            }
        }
        public List<SubProgramSetupModel> PopulateSubProgramList()
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                List<SubProgramSetupModel> SubProgramList = new List<SubProgramSetupModel>();
                SubProgramList = db.Database.SqlQuery<SubProgramSetupModel>("PopulateSubProgramSetup").ToList();
                return SubProgramList;
            }
        }


        public string InsertProgramSetup(ProgramSetupModel _Model)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                string msg = string.Empty;
                byte[] img = new byte[0];
                _Model.ObjProgramSetupMasterModel.Status = true;
                int CurrentLoginUserid = CommonUtilities.GetCurrentLoginUserId();
                var ProgramNameParam = new SqlParameter { ParameterName = "@ProgramName", Value = _Model.ObjProgramSetupMasterModel.ProgramName };
                var ShortDescriptionParam = new SqlParameter { ParameterName = "@Remarks", Value = _Model.ObjProgramSetupMasterModel.Remarks };
                var StartDateParam = new SqlParameter { ParameterName = "@StartDate", Value = _Model.ObjProgramSetupMasterModel.StartDate };
                var EndDateParam = new SqlParameter { ParameterName = "@EndDate", Value = _Model.ObjProgramSetupMasterModel.EndDate };
                var createdDateParam = new SqlParameter { ParameterName = "@CreatedDate", Value = DateTime.Now };
                var StatusParam = new SqlParameter { ParameterName = "@Status", Value = _Model.ObjProgramSetupMasterModel.Status };
                var CreatedByParam = new SqlParameter { ParameterName = "@CreatedBy", Value = CurrentLoginUserid };
                var MessageParam = new SqlParameter
                {
                    ParameterName = "@Message",
                    DbType = DbType.String,
                    Size = 50,
                    Direction = System.Data.ParameterDirection.Output
                };

                //var PrimaryIdParam = new SqlParameter
                //{
                //    ParameterName = "@PrimaryId",
                //    DbType = DbType.Int32,
                //    Direction = System.Data.ParameterDirection.Output
                //};


                var result = db.Database.ExecuteSqlCommand("exec InsertProgramSetup @ProgramName,@StartDate,@EndDate,@Remarks,@CreatedDate,@CreatedBy,@Status, @Message OUT", ProgramNameParam, StartDateParam, EndDateParam, ShortDescriptionParam, createdDateParam, CreatedByParam, StatusParam, MessageParam);
                //int PKID = (int)PrimaryIdParam.Value;
                //if (PKID > 0)
                //{
                //    //_Model.MetaDetails.SectionId = 22;
                //    //_Model.MetaDetails.PKID = PKID;
                //    //InsertMetaTagDetails(_Model.MetaDetails);
                //}
                msg = MessageParam.SqlValue.ToString();

                if (msg == "Saved Successfully")
                {
                    //string Password = "123456";
                    //int userType = 1;
                    //WebSecurity.CreateUserAndAccount(_Model.EmailId, Password, new { UserType = userType, EmailID = _Model.EmailId, Status = true, RoleTypeId = 2 });
                    //string RoleName = "AgencySetup";
                    //Roles.AddUserToRole(_Model.EmailId, RoleName);
                }

                return msg;

            }
        }


        public string UpdateProgramSetup(ProgramSetupModel _Model)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                string msg = string.Empty;
                byte[] img = new byte[0];
                _Model.ObjProgramSetupMasterModel.Status = true;
                var ProgramIdParam = new SqlParameter { ParameterName = "@programId", Value = _Model.ObjProgramSetupMasterModel.ProgramSetupId };
                var ProgramNameParam = new SqlParameter { ParameterName = "@ProgramName", Value = _Model.ObjProgramSetupMasterModel.ProgramName };
                var StartDateParam = new SqlParameter { ParameterName = "@StartDate", Value = _Model.ObjProgramSetupMasterModel.StartDate };
                var EndDateParam = new SqlParameter { ParameterName = "@EndDate", Value = _Model.ObjProgramSetupMasterModel.EndDate };
                var ShortDescriptionParam = new SqlParameter { ParameterName = "@Remarks", Value = _Model.ObjProgramSetupMasterModel.Remarks };
                var MessageParam = new SqlParameter
                {
                    ParameterName = "@Message",
                    DbType = DbType.String,
                    Size = 50,
                    Direction = System.Data.ParameterDirection.Output
                };

                //var PrimaryIdParam = new SqlParameter
                //{
                //    ParameterName = "@PrimaryId",
                //    DbType = DbType.Int32,
                //    Direction = System.Data.ParameterDirection.Output
                //};


                var result = db.Database.ExecuteSqlCommand("exec UpdateProgramSetup @programId, @ProgramName,@StartDate,@EndDate,@Remarks, @Message OUT", ProgramIdParam, ProgramNameParam, StartDateParam, EndDateParam, ShortDescriptionParam, MessageParam);
                //int PKID = (int)PrimaryIdParam.Value;
                //if (PKID > 0)
                //{
                //    //_Model.MetaDetails.SectionId = 22;
                //    //_Model.MetaDetails.PKID = PKID;
                //    //InsertMetaTagDetails(_Model.MetaDetails);
                //}
                msg = MessageParam.SqlValue.ToString();

                if (msg == "Saved Successfully")
                {
                    //string Password = "123456";
                    //int userType = 1;
                    //WebSecurity.CreateUserAndAccount(_Model.EmailId, Password, new { UserType = userType, EmailID = _Model.EmailId, Status = true, RoleTypeId = 2 });
                    //string RoleName = "AgencySetup";
                    //Roles.AddUserToRole(_Model.EmailId, RoleName);
                }

                return msg;

            }
        }


        public string InsertSubProgramSetup(ProgramSetupModel _Model)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                string msg = string.Empty;
                byte[] img = new byte[0];
                _Model.ObjSubProgramSetupModel.Status = true;
                int CurrentLoginUserid = CommonUtilities.GetCurrentLoginUserId();
                var ProgramSetupIdParam = new SqlParameter { ParameterName = "@ProgramSetupId", Value = _Model.ObjSubProgramSetupModel.ProgramSetupId };
                var SubProgramNameParam = new SqlParameter { ParameterName = "@SubProgramName", Value = _Model.ObjSubProgramSetupModel.SubProgramName };
                var ShortDescriptionParam = new SqlParameter { ParameterName = "@Remarks", Value = _Model.ObjSubProgramSetupModel.Remarks };
                var createdDateParam = new SqlParameter { ParameterName = "@CreatedDate", Value = DateTime.Now };
                var StatusParam = new SqlParameter { ParameterName = "@Status", Value = _Model.ObjSubProgramSetupModel.Status };
                var CreatedByParam = new SqlParameter { ParameterName = "@CreatedBy", Value = CurrentLoginUserid };
                var MessageParam = new SqlParameter
                {
                    ParameterName = "@Message",
                    DbType = DbType.String,
                    Size = 50,
                    Direction = System.Data.ParameterDirection.Output
                };

                var PrimaryIdParam = new SqlParameter
                {
                    ParameterName = "@PrimaryId",
                    DbType = DbType.Int32,
                    Direction = System.Data.ParameterDirection.Output
                };


                var result = db.Database.ExecuteSqlCommand("exec InserSubProgramSetup @ProgramSetupId,@SubProgramName,@Remarks,@CreatedDate,@CreatedBy,@Status, @Message OUT,@PrimaryId OUT", ProgramSetupIdParam, SubProgramNameParam, ShortDescriptionParam, createdDateParam, CreatedByParam, StatusParam, MessageParam, PrimaryIdParam);
                int PKID = (int)PrimaryIdParam.Value;
                //if (PKID > 0)
                //{
                //    //_Model.MetaDetails.SectionId = 22;
                //    //_Model.MetaDetails.PKID = PKID;
                //    //InsertMetaTagDetails(_Model.MetaDetails);
                //}
                msg = MessageParam.SqlValue.ToString();

                if (msg == "Saved Successfully")
                {
                    //Insert Subprogram geo location details
                    if (_Model.SubProgramGeoDetailsModelList.Count > 0)
                    {
                        foreach (var item in _Model.SubProgramGeoDetailsModelList)
                        {
                            var objToInsert = new SubProgramGeoDetails()
                            {
                                AgencyId = _Model.ObjSubProgramGeoDetailsModel.AgencyId,
                                Status = item.Status,
                                DistrictId = item.DistrictId,
                                SubProgramId = PKID
                            };
                            db.SubProgramGeoDetails.Add(objToInsert);

                        }

                        db.SaveChanges();
                    }


                    //string Password = "123456";
                    //int userType = 1;
                    //WebSecurity.CreateUserAndAccount(_Model.EmailId, Password, new { UserType = userType, EmailID = _Model.EmailId, Status = true, RoleTypeId = 2 });
                    //string RoleName = "AgencySetup";
                    //Roles.AddUserToRole(_Model.EmailId, RoleName);
                }

                return msg;

            }
        }

        public string UpdateSubProgramSetup(ProgramSetupModel _Model)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                string msg = string.Empty;
                byte[] img = new byte[0];
                _Model.ObjSubProgramSetupModel.Status = true;

                var SubProgramSetupIdParam = new SqlParameter { ParameterName = "@SubProgramSetupId", Value = _Model.ObjSubProgramSetupModel.SubProgramSetupId };
                var ProgramSetupIdParam = new SqlParameter { ParameterName = "@ProgramSetupId", Value = _Model.ObjSubProgramSetupModel.ProgramSetupId };
                var SubProgramNameParam = new SqlParameter { ParameterName = "@SubProgramName", Value = _Model.ObjSubProgramSetupModel.SubProgramName };
                var ShortDescriptionParam = new SqlParameter { ParameterName = "@Remarks", Value = _Model.ObjSubProgramSetupModel.Remarks };
                var MessageParam = new SqlParameter
                {
                    ParameterName = "@Message",
                    DbType = DbType.String,
                    Size = 50,
                    Direction = System.Data.ParameterDirection.Output
                };

                var result = db.Database.ExecuteSqlCommand("exec UpdateSubProgramSetup @SubProgramSetupId, @ProgramSetupId,@SubProgramName,@Remarks, @Message OUT", SubProgramSetupIdParam, ProgramSetupIdParam, SubProgramNameParam, ShortDescriptionParam, MessageParam);
                msg = MessageParam.SqlValue.ToString();

                if (msg == "Updated Successfully")
                {
                    //Delete first and insert into geo details table
                    var DeleteResult = db.SubProgramGeoDetails.Where(x => x.SubProgramId == _Model.ObjSubProgramSetupModel.SubProgramSetupId).ToList();
                    if (DeleteResult.Count > 0)
                    {
                        foreach (var item in DeleteResult)
                        {
                            db.SubProgramGeoDetails.Remove(item);
                        }
                        db.SaveChanges();
                    }
                    //insert

                    if (_Model.SubProgramGeoDetailsModelList.Count > 0 || _Model.SubProgramGeoDetailsModelList != null)
                    {
                        foreach (var item in _Model.SubProgramGeoDetailsModelList)
                        {
                            var objToInsert = new SubProgramGeoDetails()
                            {
                                AgencyId = _Model.ObjSubProgramGeoDetailsModel.AgencyId,
                                Status = item.Status,
                                DistrictId = item.DistrictId,
                                SubProgramId = _Model.ObjSubProgramSetupModel.SubProgramSetupId
                            };
                            db.SubProgramGeoDetails.Add(objToInsert);

                        }
                        db.SaveChanges();


                    }
                }
                return msg;

            }
        }


        public List<SubProgramGeoDetailsModel> GetWorkingAreaDetails(int AgencyId)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                List<SubProgramGeoDetailsModel> DistrictModelList = new List<SubProgramGeoDetailsModel>();
                var DistrictList = db.Database.SqlQuery<SubProgramGeoDetailsModel>(@"Select DistrictId,Stauts as Status From AgencyGeographicCoverage where AgencyId=" + AgencyId).ToList();
                foreach (var item in DistrictList)
                {
                    if (item.Status == true)
                    {
                        SubProgramGeoDetailsModel model = new SubProgramGeoDetailsModel();
                        model.DistrictId = item.DistrictId;
                        model.DistrictName = CommonUtilities.GetProvinecesDistrictByDistrictIdNew(item.DistrictId);
                        model.Status = item.Status;
                        DistrictModelList.Add(model);
                    }
                }
                return DistrictModelList;
            }
        }


        public List<SubProgramGeoDetailsModel> GetWorkingAreaDetailsBySubProgramId(int SubProgramId)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                List<SubProgramGeoDetailsModel> DistrictModelList = new List<SubProgramGeoDetailsModel>();
                var DistrictList = db.Database.SqlQuery<SubProgramGeoDetailsModel>(@"select DistrictId,Status From SubProgramGeoDetails where SubProgramId=" + SubProgramId).ToList();
                foreach (var item in DistrictList)
                {

                    SubProgramGeoDetailsModel model = new SubProgramGeoDetailsModel();
                    model.DistrictId = item.DistrictId;
                    model.DistrictName = CommonUtilities.GetProvinecesDistrictByDistrictIdNew(item.DistrictId);
                    model.Status = item.Status;
                    DistrictModelList.Add(model);

                }
                return DistrictModelList;
            }
        }



    }
}