﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PersonalSecurity.Areas.Admin.Models;
using System.Data;
using PersonalSecurity.Models;
using WebMatrix.WebData;
using System.Web.Security;

namespace PersonalSecurity.Areas.Admin.Providers
{
    public class EmployeeRecordsProviders
    {

        public List<EmployeeRecordModel> GetList()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                List<EmployeeRecordModel> Returnlist = ent.EmployeeRecords.Select(x => new EmployeeRecordModel()
                {
                    EmployeeId = x.EmployeeId,
                    FullNameEng = x.FullNameEng,
                    FullNameNep = x.FullNameNep,
                    GenderId = x.GenderId,
                    DepartmentId = x.DepartmentId,
                    DesignationId = x.DepartmentId,
                    Address = x.Address,
                    Status = x.Status,
                    MartialStatus = x.MartialStatus,
                    ContactNumber = x.ContactNumber,
                    MobileNumber = x.MobileNumber,
                    Photo = x.Photo,
                    DateOfBirth = x.DateOfBirth

                }).ToList();

                return Returnlist;
            }
        }

        public bool Insert(EmployeeRecordModel model)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var Insert = new EmployeeRecords
                {
                    FullNameEng = model.FullNameEng,
                    FullNameNep = model.FullNameNep,
                    DateOfBirth = model.DateOfBirth,
                    Status = model.Status,
                    GenderId = model.GenderId,
                    DepartmentId = model.DepartmentId,
                    DesignationId = model.DesignationId,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now,
                    UpdatedBy = 1,
                    UpdatedDate = DateTime.Now,
                    MartialStatus = model.MartialStatus,
                    Address = model.Address,
                    ContactNumber = model.ContactNumber,
                    MobileNumber = model.MobileNumber
                };

                ent.EmployeeRecords.Add(Insert);
                int i = ent.SaveChanges();
                if (i > 0)
                {
                    UsersContext con = new UsersContext();
                    string rolename = ent.Database.SqlQuery<string>("Select RoleName from webpages_Roles where RoleId='" + model.RoleType + "'").FirstOrDefault();
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new { UserGroup = model.RoleType, UserType = model.UserType, EmployeeId = Insert.EmployeeId });
                    int userId = WebSecurity.GetUserId(model.UserName);
                    Roles.AddUserToRole(model.UserName, rolename);
                    con.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
        }

        public bool Edit(EmployeeRecordModel model)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var EditObj = ent.EmployeeRecords.SingleOrDefault(x => x.EmployeeId == model.EmployeeId);
                if (EditObj != null)
                {
                    EditObj.FullNameEng = model.FullNameEng;
                    EditObj.FullNameNep = model.FullNameNep;
                    EditObj.GenderId = model.GenderId;
                    EditObj.DesignationId = model.DesignationId;
                    EditObj.DepartmentId = model.DepartmentId;
                    EditObj.MartialStatus = model.MartialStatus;
                    EditObj.UpdatedDate = DateTime.Now;
                    EditObj.UpdatedBy = 1;
                    EditObj.Address = model.Address;
                    EditObj.ContactNumber = model.ContactNumber;
                    EditObj.MobileNumber = model.MobileNumber;
                    ent.Entry(EditObj).State = EntityState.Modified;
                    int i = ent.SaveChanges();
                    if (i > 0)
                    {
                        //UsersContext con = new UsersContext();
                        //string rolename = ent.Database.SqlQuery<string>("Select RoleName from webpages_Roles where RoleId='" + model.RoleType + "'").FirstOrDefault();
                        //WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new { UserGroup = model.RoleType, UserType = model.UserType, UserLoginStatus = 1 });
                        //int userId = WebSecurity.GetUserId(model.UserName);
                        //Guid UserUNQRow = ent.Database.SqlQuery<Guid>("Select UserUNQRow from UserProfile where UserId='" + userId + "'").FirstOrDefault();
                        //Roles.AddUserToRole(model.UserName, rolename);
                        //con.SaveChanges();


                        return true;
                    }
                    else
                        return false;

                }
                else
                    return false;
            }
        }


        public bool Delete(int id)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var DeleteObj = ent.EmployeeRecords.SingleOrDefault(x => x.EmployeeId == id);
                if (DeleteObj != null)
                {
                    ent.EmployeeRecords.Remove(DeleteObj);
                    int i = ent.SaveChanges();
                    if (i > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
        }
    }
}