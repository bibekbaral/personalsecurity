﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PersonalSecurity.Areas.Admin.Models;

namespace PersonalSecurity.Areas.Admin.Providers
{
    public class UserTypeSetupProviders
    {

        public List<UserTypeModel> GetUserTypeList()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                List<UserTypeModel> ReturnList = ent.UserType.Select(x => new UserTypeModel()
                    {
                        IsApproved = x.IsApproved,
                        UserTypeId = x.UserTypeId,
                        UserTypeName = x.UserTypeName

                    }).ToList();

                return ReturnList;

            }
        }
        public bool InsertUserType(UserTypeModel model)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var objInsertUserType = new UserType
                {
                    UserTypeName = model.UserTypeName,
                    CreatedDate = DateTime.Now,
                    CreatedBy = 1,
                    IsApproved = model.IsApproved,
                    ModifiedDate = DateTime.Now,

                };
                ent.UserType.Add(objInsertUserType);
                int i = ent.SaveChanges();

                if (i > 0)

                    return true;

                else
                    return false;
            }
        }

        public bool EditUserType(int id, UserTypeModel model)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var objEditUserType = ent.UserType.SingleOrDefault(x => x.UserTypeId == id);
                if (objEditUserType != null)
                {
                    objEditUserType.UserTypeName = model.UserTypeName;
                    objEditUserType.ModifiedDate = DateTime.Now;
                    objEditUserType.IsApproved = model.IsApproved;
                    int i = ent.SaveChanges();
                    if (i > 0)

                        return true;

                    else
                        return false;

                }
                else
                {
                    return false;
                }


            }
        }

        public bool DeleteUserType(int id)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var objDeleteUserType = ent.UserType.SingleOrDefault(x => x.UserTypeId == id);
                if (objDeleteUserType != null)
                {
                    ent.UserType.Remove(objDeleteUserType);
                    int i = ent.SaveChanges();
                    if (i > 0)
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;

                }


            }
        }




    }
}