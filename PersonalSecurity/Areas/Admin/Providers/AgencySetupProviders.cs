﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PersonalSecurity.Areas.Admin.Models;
using System.Data.SqlClient;
using System.Data;
using WebMatrix.WebData;
using System.Web.Security;

namespace PersonalSecurity.Areas.Admin.Providers
{
    public class AgencySetupProviders
    {
        public List<AgencySetupModel> PopulateAgencySetupList()
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                List<AgencySetupModel> AgencyList = new List<AgencySetupModel>();
                AgencyList = db.Database.SqlQuery<AgencySetupModel>("PopulateAgencySetup").ToList();
                return AgencyList;
            }
        }

        public string insertAgencySetup(AgencySetupModel _Model)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                string msg = string.Empty;
                byte[] img = new byte[0];


                var AgencyNameParam = new SqlParameter { ParameterName = "@AgencyName", Value = _Model.AgencyName };
                var ProvincesIdParam = new SqlParameter { ParameterName = "@ProvincesId", Value = _Model.ProvincesId };
                var ProvincesDistrictIdParam = new SqlParameter { ParameterName = "@ProvincesDistrictId", Value = _Model.ProvincesDistrictId };
                var ProvincesVDCIdParam = new SqlParameter { ParameterName = "@ProvincesVDCId", Value = _Model.ProvincesVDCId };
                var WardNumberParam = new SqlParameter { ParameterName = "@WardNumber", Value = _Model.WardNumber };
                var EmailIdParam = new SqlParameter { ParameterName = "@EmailId", Value = _Model.EmailId };
                var ContactNumberParam = new SqlParameter { ParameterName = "@ContactNumber", Value = _Model.ContactNumber };
                var ShortDescriptionParam = new SqlParameter { ParameterName = "@ShortDescription", Value = _Model.ShortDescription };


                var createdDateParam = new SqlParameter { ParameterName = "@CreatedDate", Value = DateTime.Now };
                var StatusParam = new SqlParameter { ParameterName = "@Status", Value = _Model.Status };

                //var ImagePathParam = new SqlParameter { ParameterName = "@ImagePath", Value = _Model.ServicesMain.ImagePath == null ? string.Empty : _Model.ServicesMain.ImagePath };
                //var IconImageParam = new SqlParameter { ParameterName = "@IconImage", Value = _Model.ServicesMain.IconImage == null ? img : _Model.ServicesMain.IconImage };
                //var ContactNumberparam = new SqlParameter { ParameterName = "@ContactNumber", Value = _Model.ServicesMain.ContactNumber == null ? string.Empty : _Model.ServicesMain.ContactNumber };

                var MessageParam = new SqlParameter
                {
                    ParameterName = "@Message",
                    DbType = DbType.String,
                    Size = 50,
                    Direction = System.Data.ParameterDirection.Output
                };

                var PrimaryIdParam = new SqlParameter
                {
                    ParameterName = "@PrimaryId",
                    DbType = DbType.Int32,
                    Direction = System.Data.ParameterDirection.Output
                };


                var result = db.Database.ExecuteSqlCommand("exec InsertAgencySetup @AgencyName,@ProvincesId,@ProvincesDistrictId,@ProvincesVDCId,@WardNumber,@EmailId,@ContactNumber,@ShortDescription,@CreatedDate,@Status, @Message OUT,@PrimaryId OUT", AgencyNameParam, ProvincesIdParam, ProvincesDistrictIdParam, ProvincesVDCIdParam, WardNumberParam, EmailIdParam, ContactNumberParam, ShortDescriptionParam, createdDateParam, StatusParam, MessageParam, PrimaryIdParam);
                int PKID = (int)PrimaryIdParam.Value;
                //if (PKID > 0)
                //{
                //    //_Model.MetaDetails.SectionId = 22;
                //    //_Model.MetaDetails.PKID = PKID;
                //    //InsertMetaTagDetails(_Model.MetaDetails);
                //}
                msg = MessageParam.SqlValue.ToString();

                if (msg == "Saved Successfully")
                {
                    if (PKID > 0)
                    {
                        string Password = "123456";
                        int userType = 2;
                        WebSecurity.CreateUserAndAccount(_Model.EmailId, Password, new { UserType = userType, UserName = _Model.EmailId, Status = true, UserGroup = 1, AgencyId = PKID });
                        //string RoleName = "Agency";
                        //Roles.AddUserToRole(_Model.EmailId, RoleName);
                    }
                }

                return msg;

            }
        }


        public string UpdateAgencySetup(AgencySetupModel _Model)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                string msg = string.Empty;
                byte[] img = new byte[0];
                //_Model.Status = false;

                var AgencyIdParam = new SqlParameter { ParameterName = "@AgencyId", Value = _Model.AgencyId };
                var AgencyNameParam = new SqlParameter { ParameterName = "@AgencyName", Value = _Model.AgencyName };
                var ProvincesIdParam = new SqlParameter { ParameterName = "@ProvincesId", Value = _Model.ProvincesId };
                var ProvincesDistrictIdParam = new SqlParameter { ParameterName = "@ProvincesDistrictId", Value = _Model.ProvincesDistrictId };
                var ProvincesVDCIdParam = new SqlParameter { ParameterName = "@ProvincesVDCId", Value = _Model.ProvincesVDCId };
                var WardNumberParam = new SqlParameter { ParameterName = "@WardNumber", Value = _Model.WardNumber };
                var EmailIdParam = new SqlParameter { ParameterName = "@EmailId", Value = _Model.EmailId };
                var ContactNumberParam = new SqlParameter { ParameterName = "@ContactNumber", Value = _Model.ContactNumber };
                var ShortDescriptionParam = new SqlParameter { ParameterName = "@ShortDescription", Value = _Model.ShortDescription };


                //var createdDateParam = new SqlParameter { ParameterName = "@CreatedDate", Value = DateTime.Now };
                //var StatusParam = new SqlParameter { ParameterName = "@Status", Value = _Model.Status };

                //var ImagePathParam = new SqlParameter { ParameterName = "@ImagePath", Value = _Model.ServicesMain.ImagePath == null ? string.Empty : _Model.ServicesMain.ImagePath };
                //var IconImageParam = new SqlParameter { ParameterName = "@IconImage", Value = _Model.ServicesMain.IconImage == null ? img : _Model.ServicesMain.IconImage };
                //var ContactNumberparam = new SqlParameter { ParameterName = "@ContactNumber", Value = _Model.ServicesMain.ContactNumber == null ? string.Empty : _Model.ServicesMain.ContactNumber };

                var MessageParam = new SqlParameter
                {
                    ParameterName = "@Message",
                    DbType = DbType.String,
                    Size = 50,
                    Direction = System.Data.ParameterDirection.Output
                };

                //var PrimaryIdParam = new SqlParameter
                //{
                //    ParameterName = "@PrimaryId",
                //    DbType = DbType.Int32,
                //    Direction = System.Data.ParameterDirection.Output
                //};


                var result = db.Database.ExecuteSqlCommand("exec UpdateAgencySetup @AgencyId, @AgencyName,@ProvincesId,@ProvincesDistrictId,@ProvincesVDCId,@WardNumber,@EmailId,@ContactNumber,@ShortDescription, @Message OUT", AgencyIdParam, AgencyNameParam, ProvincesIdParam, ProvincesDistrictIdParam, ProvincesVDCIdParam, WardNumberParam, EmailIdParam, ContactNumberParam, ShortDescriptionParam, MessageParam);
                //int PKID = (int)PrimaryIdParam.Value;
                //if (PKID > 0)
                //{
                //    //_Model.MetaDetails.SectionId = 22;
                //    //_Model.MetaDetails.PKID = PKID;
                //    //InsertMetaTagDetails(_Model.MetaDetails);
                //}
                msg = MessageParam.SqlValue.ToString();

                if (msg == "Updated Successfully")
                {
                    //string Password = "123456";
                    //int userType = 1;
                    //WebSecurity.CreateUserAndAccount(_Model.EmailId, Password, new { UserType = userType, EmailID = _Model.EmailId, Status = true, RoleTypeId = 2 });
                    //string RoleName = "AgencySetup";
                    //Roles.AddUserToRole(_Model.EmailId, RoleName);
                }

                return msg;

            }
        }





        public bool IsAlreadyEmailUsed(string EmailId)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                return false;
            }
        }

        public bool IsAgencyNameAlreadyExist(string AgencyName)
        {
            return false;
        }
        public bool IsAgencyAlreadyInsertedGeoDetails(int AgencyId)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                bool IsInserted = db.AgencyGeographicCoverage.Any(x => x.AgencyId == AgencyId);

                return IsInserted;
            }
        }


        public List<AgencyGeographicCoverageModel> GetDistrictList()
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                List<AgencyGeographicCoverageModel> DistrictModelList = new List<AgencyGeographicCoverageModel>();
                var DistrictList = db.Database.SqlQuery<AgencyGeographicCoverageModel>(@"select NameNepali as DistrictName, DistrictCode as DistrictId from District").ToList();
                foreach (var item in DistrictList)
                {
                    AgencyGeographicCoverageModel model = new AgencyGeographicCoverageModel();
                    model.DistrictId = item.DistrictId;
                    model.DistrictName = item.DistrictName;
                    DistrictModelList.Add(model);

                }
                return DistrictModelList;

            }
        }

        public List<AgencyGeographicCoverageModel> GetDistrictListForEdit(int AgencyId)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                List<AgencyGeographicCoverageModel> DistrictModelList = new List<AgencyGeographicCoverageModel>();
                var DistrictList = db.Database.SqlQuery<AgencyGeographicCoverageModel>(@"Select DistrictId,Stauts as IsChecked From AgencyGeographicCoverage where AgencyId=" + AgencyId).ToList();
                foreach (var item in DistrictList)
                {
                    AgencyGeographicCoverageModel model = new AgencyGeographicCoverageModel();
                    model.DistrictId = item.DistrictId;
                    model.DistrictName = CommonUtilities.GetProvinecesDistrictByDistrictIdNew(item.DistrictId);
                    model.IsChecked = item.IsChecked;
                    DistrictModelList.Add(model);
                }
                return DistrictModelList;
            }
        }


        public bool InsertGeoCoverageOfAgency(AgencySetupModel model)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                //First Delete All District and delete
                var ObjToDelete = db.AgencyGeographicCoverage.Where(x => x.AgencyId == model.AgencyId).ToList();

                if (ObjToDelete.Count > 0)
                {

                    foreach (var item in ObjToDelete)
                    {
                        db.AgencyGeographicCoverage.Remove(item);
                    }
                    db.SaveChanges();
                }


                if (model.AgencyGeographicCoverageModelList != null && model.AgencyGeographicCoverageModelList.Count > 0)
                {
                    foreach (var item in model.AgencyGeographicCoverageModelList)
                    {

                        var ObjToInsert = new AgencyGeographicCoverage()
                        {
                            AgencyId = model.AgencyId,
                            DistrictId = item.DistrictId,
                            Stauts = item.IsChecked


                        };
                        db.AgencyGeographicCoverage.Add(ObjToInsert);

                    }
                    db.SaveChanges();
                }
            }
            return false;
        }

    }
}