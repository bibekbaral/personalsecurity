﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PersonalSecurity.Areas.Admin.Models;
using System.Data;
namespace PersonalSecurity.Areas.Admin.Providers
{
    public class DesignationSetupProviders
    {

        public List<DesignationModel> GetList()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                List<DesignationModel> ReturnList = ent.DesignationRecords.Select(x => new DesignationModel()
                {
                    DesignationFor=x.DesignationFor,
                    DesignationSetupId=x.DesignationSetupId,
                    DesignationNameEng=x.DesignationNameEng,
                    DesignationNameNep=x.DesignationNameNep,
                    Status=x.Status
                }).ToList();

                return ReturnList;
            }
        }

        public bool Insert(DesignationModel model)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var InsertObj = new DesignationRecords
                {
                    DesignationNameEng=model.DesignationNameEng,
                    DesignationNameNep=model.DesignationNameNep,
                    Status=model.Status

                };
                ent.DesignationRecords.Add(InsertObj);
                int i = ent.SaveChanges();
                if (i > 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }


        public bool Edit(DesignationModel model)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var EditObj = ent.DesignationRecords.SingleOrDefault(x => x.DesignationSetupId == model.DesignationSetupId);
                if (EditObj != null)
                {
                    EditObj.DesignationNameEng = model.DesignationNameEng;
                    EditObj.DesignationNameNep = model.DesignationNameNep;
                    EditObj.Status = model.Status;
                    ent.Entry(EditObj).State = EntityState.Modified;
                    int i = ent.SaveChanges();
                    if (i > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
        }


    }
}