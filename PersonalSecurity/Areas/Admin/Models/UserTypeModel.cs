﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PersonalSecurity.Areas.Admin.Models
{
    public class UserTypeModel
    {
        public int UserTypeId { get; set; }
        [Required(ErrorMessage = "This field is required")]
        [DisplayName("प्रयोगकर्ता प्रकार")]        
        public string UserTypeName { get; set; }
        [Required(ErrorMessage = "This field is required")]
        [DisplayName("Status")]        
        public bool IsApproved { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public List<UserTypeModel> UserTypeList { get; set; }

    }
}