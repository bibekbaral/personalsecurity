﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PersonalSecurity.Areas.Admin.Models
{
    public class DesignationModel
    {
        public int DesignationSetupId { get; set; }
         [Display(Name = "श्रेणी/तह(अंग्रेजी)")]
        [Required]
        public string DesignationNameEng { get; set; }
        [Display(Name = "श्रेणी/तह(नेपाली)")]
        [Required]
        public string DesignationNameNep { get; set; }
         [Display(Name = "स्थिति")]
        public bool Status { get; set; }
        public int DesignationFor { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }


        public List<DesignationModel> DesignationList { get; set; }


    }
}