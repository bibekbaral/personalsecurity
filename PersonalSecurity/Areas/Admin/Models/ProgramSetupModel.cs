﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PersonalSecurity.Areas.Admin.Models
{
    public class ProgramSetupModel
    {
        public ProgramSetupMasterModel ObjProgramSetupMasterModel { get; set; }
        public SubProgramSetupModel ObjSubProgramSetupModel { get; set; }

        public List<ProgramSetupMasterModel> ProgramSetupMasterModelList { get; set; }
        public List<SubProgramSetupModel> SubProgramSetupModelList { get; set; }

        public SubProgramGeoDetailsModel ObjSubProgramGeoDetailsModel { get; set; }
        public List<SubProgramGeoDetailsModel> SubProgramGeoDetailsModelList { get; set; }

        public ProgramSetupModel()
        {
            ObjProgramSetupMasterModel = new ProgramSetupMasterModel();
            ProgramSetupMasterModelList = new List<ProgramSetupMasterModel>();
            ObjSubProgramSetupModel = new SubProgramSetupModel();
            SubProgramSetupModelList = new List<SubProgramSetupModel>();

            ObjSubProgramGeoDetailsModel = new SubProgramGeoDetailsModel();
            SubProgramGeoDetailsModelList = new List<SubProgramGeoDetailsModel>();
        }

    }

    public class ProgramSetupMasterModel
    {

        public int ProgramSetupId { get; set; }
        [Display(Name = "कार्यक्रमको नाम")]
        public string ProgramName { get; set; }
        [Display(Name = "अवस्था")]
        public bool Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Display(Name = "शुरु मिति")]
        public DateTime StartDate { get; set; }
        [Display(Name = "समाप्त मिति")]
        public DateTime EndDate { get; set; }
        [Display(Name = "विवरण")]
        public string Remarks { get; set; }

    }

    public class SubProgramSetupModel
    {

        public int SubProgramSetupId { get; set; }
        [Display(Name = "Programme")]
        public int ProgramSetupId { get; set; }
        [Display(Name = "Sub-Programme Name")]
        public string SubProgramName { get; set; }
        [Display(Name = "Status")]
        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

    }

    public class SubProgramGeoDetailsModel
    {
        public int SubProgramGeoDetailsId { get; set; }
        public int AgencyId { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int SubProgramId { get; set; }
        public bool Status { get; set; }
    }
}