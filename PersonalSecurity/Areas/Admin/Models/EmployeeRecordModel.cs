﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PersonalSecurity.Areas.Admin.Models
{
    public class EmployeeRecordModel
    {

        public int EmployeeId { get; set; }
        [Required]
        [Display(Name = "नाम(अंग्रेजी)")]
        public string FullNameEng { get; set; }
        [Required]
        [Display(Name = "नाम(नेपाली)")]
        public string FullNameNep { get; set; }
        [Required]
        [Display(Name = "जन्म मिति")]
        public DateTime DateOfBirth { get; set; }
        [Required]
        [Display(Name = "वैवाहिक स्थिति")]
        public int MartialStatus { get; set; }
        [Required]
        [Display(Name = "लिङ्ग")]
        public int GenderId { get; set; }
        [Required]
        [Display(Name = "श्रेणी/तह")]
        public int DesignationId { get; set; }

        [Display(Name = "मोबाइल नम्बर")]
        public string MobileNumber { get; set; }

        [Display(Name = "सम्पर्क नम्बर")]
        public string ContactNumber { get; set; }
        [Required]
        [Display(Name = "विभाग")]
        public int DepartmentId { get; set; }
        [Required]
        [Display(Name = "ठेगाना")]
        public string Address { get; set; }
        public string Photo { get; set; }
        [Required]
        [Display(Name = "स्थिति")]
        public bool Status { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 5)]
        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "प्रयोगकर्ताको नाम")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "This field is required")]

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "पासवर्ड")]
        public string Password { get; set; }
        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The new password and confirmation password do not match.")]
        [Display(Name = "पासवर्ड पुन: टाइप गर्नुहोस्")]
        public string RetypePassword { get; set; }
        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "प्रयोगकर्ता प्रकार")]
        public int UserType { get; set; }
         [Display(Name = "रोल")]
        public int RoleType { get; set; }
        public string ErrorMessage { get; set; }
        public string Successmessage { get; set; }
        
        public string Readonly { get; set; }

        public string usertext { get; set; }

        [DisplayName("User Name")]
        public string Uname { get; set; }



        public List<EmployeeRecordModel> EmployeeRecordModelList { get; set; }


    }
}