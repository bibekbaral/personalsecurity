﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PersonalSecurity.Areas.Admin.Models
{
    public class DepartmentRecordsModel
    {

        public int DepartmentRecordsId { get; set; }
        [Required]
        [Display(Name = "विभाग(अंग्रेजी)")]
        public string DepartmentNameEng { get; set; }
        [Required]
        [Display(Name = "विभाग(नेपाली)")]
        public string DepartmentNameNep { get; set; }
         [Display(Name = "स्थिति")]
        public bool? Status { get; set; }

        public List<DepartmentRecordsModel> DepartmentRecordsModelList { get; set; }
    }
}