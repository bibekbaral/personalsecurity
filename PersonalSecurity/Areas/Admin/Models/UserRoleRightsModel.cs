﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PersonalSecurity.Areas.Admin.Models
{
    public class UserRoleRightsModel
    {

        public int UserRoleTypeRecordId { get; set; }
        public int GroupRole { get; set; }
        [Display(Name = "Rights")]
        public int AclActionRecordId { get; set; }
        [Display(Name = "Section")]
        public string AclControllerLabel { get; set; }
        public bool UserRoleStatus { get; set; }
        public bool IsDefault { get; set; }
        public bool isChecked { get; set; }
        public int USER_TYPE_CD { get; set; }
        [Display(Name = "Permission")]
        public string AclActionLabel { get; set; }
        public int AclControllerRecordId { get; set; }
        public List<UserRoleRightsModel> UserRoleRightsModelList { get; set; }

        public List<UserRoleRightsModel> UserRoleRightsModelMainList { get; set; }


        public bool SelectAll { get; set; }

        public bool SelectDiv { get; set; }

    }
}