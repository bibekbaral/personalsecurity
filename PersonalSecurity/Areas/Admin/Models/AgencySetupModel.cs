﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PersonalSecurity.Areas.Admin.Models
{
    public class AgencySetupModel
    {
        public int AgencyId { get; set; }


        [Display(Name = "निकायको नाम")]
        public string AgencyName { get; set; }
        [Display(Name = "प्रदेश")]
        public int ProvincesId { get; set; }
        [Display(Name = "जिल्ला")]
        public int ProvincesDistrictId { get; set; }
        [Display(Name = "न.पा/गाविस")]
        public int ProvincesVDCId { get; set; }
        [Display(Name = "वडा नं")]
        public int WardNumber { get; set; }
        [Display(Name = "ईमेल / प्रयोगकर्ता")]
        public string EmailId { get; set; }
        [Display(Name = "सम्पर्क नं.")]
        public string ContactNumber { get; set; }
        [Display(Name = "विवरण")]
        public string ShortDescription { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool Status { get; set; }



        public AgencySetupModel ObjAgencySetupModel { get; set; }
        public List<AgencySetupModel> AgencySetupModelList { get; set; }


        public AgencyGeographicCoverageModel objAgencyGeographicCoverageModel { get; set; }
        public List<AgencyGeographicCoverageModel> AgencyGeographicCoverageModelList { get; set; }
        //public AgencySetupModel()
        //{
        //    ObjAgencySetupModel = new AgencySetupModel();
        //    AgencySetupModelList = new List<AgencySetupModel>();

        //}

    }

    public class AgencyGeographicCoverageModel
    {
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public bool IsChecked { get; set; }
    }



}