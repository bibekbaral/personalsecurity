﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PersonalSecurity.Areas.Agency.Models;
using PersonalSecurity.Areas.Agency.Providers;

namespace PersonalSecurity.Areas.Agency.Controllers
{
    public class KindDistributionController : Controller
    {
        //
        // GET: /Agency/KindDistribution/

        KindDistributionProviders pro = new KindDistributionProviders();
        public ActionResult Index()
        {
            KindDistributionDetailModel model = new KindDistributionDetailModel();
            model.KindDistributionDetailModelList = pro.PopulateKindDistributionList();
            return View(model);
        }

        public ActionResult Create()
        {
            KindDistributionDetailModel model = new KindDistributionDetailModel();
            return View(model);
        }


        [HttpPost]
        public ActionResult Create(KindDistributionDetailModel model)
        {
            int AgencyId = CommonUtilities.GetCurrentLoginUserId();//AgencyId
            model.AgencyId = AgencyId;
            model.Status = true;
            pro.InsertKindDistribution(model);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            KindDistributionDetailModel model = new KindDistributionDetailModel();
            model = pro.PopulateKindDistributionList().SingleOrDefault(x => x.KindDistributionId == id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(KindDistributionDetailModel model)
        {
            //int AgencyId = CommonUtilities.GetCurrentLoginUserId();//AgencyId
            pro.UpdateKindDistribution(model);
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            return RedirectToAction("Index");
        }


    }
}
