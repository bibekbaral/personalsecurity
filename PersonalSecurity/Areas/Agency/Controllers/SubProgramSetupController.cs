﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PersonalSecurity.Areas.Admin.Models;
using PersonalSecurity.Areas.Admin.Providers;

namespace PersonalSecurity.Areas.Agency.Controllers
{
    public class SubProgramSetupController : Controller
    {
        //
        // GET: /Admin/SubProgramSetup/

        ProgramSetupProviders pro = new ProgramSetupProviders();
        public ActionResult Index()
        {
            ProgramSetupModel model = new ProgramSetupModel();
            model.SubProgramSetupModelList = pro.PopulateSubProgramList();
            return View(model);
        }

        public ActionResult Create()
        {
            ProgramSetupModel model = new ProgramSetupModel();
            int? AgencyId = CommonUtilities.GetCurrentLoginAgencyId();
            model.SubProgramGeoDetailsModelList = pro.GetWorkingAreaDetails(Convert.ToInt32(AgencyId));
            model.ObjSubProgramGeoDetailsModel.AgencyId = Convert.ToInt32(AgencyId);

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ProgramSetupModel model)
        {
            try
            {
                pro.InsertSubProgramSetup(model);
                TempData["msg"] = "Sub program saved successfully.";
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Error...Please try again.";
                return View(model);
            }

            return RedirectToAction("Index");
        }
        public ActionResult Edit(int id)
        {
            ProgramSetupModel model = new ProgramSetupModel();
            int? AgencyId = CommonUtilities.GetCurrentLoginAgencyId();
            model.ObjSubProgramSetupModel = pro.PopulateSubProgramList().SingleOrDefault(x => x.SubProgramSetupId == id);
            model.SubProgramGeoDetailsModelList = pro.GetWorkingAreaDetailsBySubProgramId(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(ProgramSetupModel model)
        {
            pro.UpdateSubProgramSetup(model);
            TempData["msg"] = "Sub program edited successfully.";
            return RedirectToAction("Index");
        }

    }
}
