﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PersonalSecurity.Areas.Agency.Models;
using PersonalSecurity.Areas.Agency.Providers;

namespace PersonalSecurity.Areas.Agency.Controllers
{
    public class DistributionController : Controller
    {
        //
        // GET: /Agency/Distribution/

        DistributionsProviders pro = new DistributionsProviders();
        public ActionResult Index()
        {
            DistributionDetailsModel model = new DistributionDetailsModel();
            int AgencyId = CommonUtilities.GetCurrentLoginUserId();
            model.DistributionDetailsModelList = pro.PopulateDistributionList().Where(x => x.AgencyId == AgencyId).ToList();
            return View(model);
        }

        public ActionResult AddDistribution(string id)
        {
            DistributionDetailsModel model = new DistributionDetailsModel();            
            model.objHouseHoldModel.householdID1 = id;
            model.AgencyId = CommonUtilities.GetCurrentLoginUserId();
            model.objHouseHoldModel = pro.PopulateHouseholdDetailsHouseHoldId(model).FirstOrDefault();            
            return View(model);
        }
        [HttpPost]
        public ActionResult AddDistribution(DistributionDetailsModel model)
        {
           
            model.Status = true;
            pro.InsertDistribution(model);
            return RedirectToAction("Index");
        }


    }





}




