﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PersonalSecurity.Areas.Agency.Controllers
{
    public class AjaxCallController : Controller
    {
        //
        // GET: /Admin/AjaxCall/

        public ActionResult Index()
        {
            return View();
        }

        public class ReturnViewmodel
        {
            public int PrimaryKeyId { get; set; }
            public string ValueName { get; set; }
        }


        public ActionResult GetProvincesDistrictByProId(int id)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                List<SelectListItem> ddlList = new List<SelectListItem>();

                var collection = ent.Database.SqlQuery<ReturnViewmodel>(@"select DistrictCode as PrimaryKeyId,NameNepali as ValueName From District where StateId='" + id + "' ").ToList();
                //var collection = ent.ProvincesDistrictSetup.Where(x => x.ProvincesId == id);

                foreach (var item in collection)
                {
                    ddlList.Add(new SelectListItem { Text = item.ValueName.ToString(), Value = item.PrimaryKeyId.ToString() });
                }
                var ddlSelectOptionList = ddlList;
                return Json(ddlSelectOptionList, JsonRequestBehavior.AllowGet);
            }
        }





        public ActionResult GetProvincesDistrictByProIdNew(int id)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                List<SelectListItem> ddlList = new List<SelectListItem>();
                var collection = ent.Database.SqlQuery<ReturnViewmodel>(@"select DistrictCode as PrimaryKeyId,NameNepali as ValueName From District where StateId='" + id + "' ").ToList();
                //var collection = ent.ProvincesDistrictSetup.Where(x => x.ProvincesId == id);

                foreach (var item in collection)
                {
                    ddlList.Add(new SelectListItem { Text = item.ValueName, Value = item.PrimaryKeyId.ToString() });
                }
                var ddlSelectOptionList = ddlList;
                return Json(ddlSelectOptionList, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetProvincesVDCByProId(int id)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                List<SelectListItem> ddlList = new List<SelectListItem>();

                var collection = ent.Database.SqlQuery<ReturnViewmodel>(@"select RuralMunicipalityId as PrimaryKeyId,NameNepali as ValueName  From RuralMunicipality where StateId='" + id + "' ").ToList();
                //var collection = ent.provincesVDCDetails.Where(x => x.ProvincesId == id);

                foreach (var item in collection)
                {
                    ddlList.Add(new SelectListItem { Text = item.ValueName, Value = item.PrimaryKeyId.ToString() });
                }
                var ddlSelectOptionList = ddlList;
                return Json(ddlSelectOptionList, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult GetProvincesVDCByDistrictId(int id, int StateId)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                List<SelectListItem> ddlList = new List<SelectListItem>();
                // var collection = ent.provincesVDCDetails.Where(x => x.ProvincesDistrictId == id);
                var collection = ent.Database.SqlQuery<ReturnViewmodel>(@"select RuralMunicipalityId as PrimaryKeyId,NameNepali as ValueName From RuralMunicipality where DistrictId='" + id + "' and StateId='" + StateId + "'").ToList();
                foreach (var item in collection)
                {
                    ddlList.Add(new SelectListItem { Text = item.ValueName, Value = item.PrimaryKeyId.ToString() });
                }
                var ddlSelectOptionList = ddlList;
                return Json(ddlSelectOptionList, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult GetSubProgramListByProgramId(int ProgramId)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                List<SelectListItem> ddlList = new List<SelectListItem>();
                int CurrentLoginUserId = CommonUtilities.GetCurrentLoginUserId();
                // var collection = ent.provincesVDCDetails.Where(x => x.ProvincesDistrictId == id);
                var collection = ent.Database.SqlQuery<ReturnViewmodel>(@"select SubProgramSetupId as PrimaryKeyId,SubProgramName as ValueName From SubProgramSetup where ProgramSetupId='" + ProgramId + "' and CreatedBy='" + CurrentLoginUserId + "'").ToList();
                foreach (var item in collection)
                {
                    ddlList.Add(new SelectListItem { Text = item.ValueName, Value = item.PrimaryKeyId.ToString() });
                }
                var ddlSelectOptionList = ddlList;
                return Json(ddlSelectOptionList, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDistributionAmountOfKind(int KindDistributionId)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var KindDistributionDetails = ent.KindDistributionDetails.Where(x => x.KindDistributionId == KindDistributionId).FirstOrDefault();
                var result = new { Amount = KindDistributionDetails.EquivalentPrice, Descriptions = KindDistributionDetails.Description };
                return Json(result, JsonRequestBehavior.AllowGet);
            }

        }





    }
}
