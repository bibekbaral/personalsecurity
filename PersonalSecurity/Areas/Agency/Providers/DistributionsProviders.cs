﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PersonalSecurity.Areas.Agency.Models;
using System.Data.SqlClient;
using System.Data;
using PersonalSecurity.Areas.HouseHold.Models;
namespace PersonalSecurity.Areas.Agency.Providers
{
    public class DistributionsProviders
    {


        public List<DistributionDetailsModel> PopulateDistributionList()
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                List<DistributionDetailsModel> DistributionList = new List<DistributionDetailsModel>();
                DistributionList = db.Database.SqlQuery<DistributionDetailsModel>("PopulateDistributions").ToList();
                return DistributionList;
            }
        }
        public string InsertDistribution(DistributionDetailsModel _Model)
        {
            if (_Model.DistributionType == 1)
            {
                //_Model.Amount = _Model.Amount;
            }
            else
            {
                //_Model.Amount = 0;//Get Equivalent amount from kinddistribution setup
            }

            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                string msg = string.Empty;
                byte[] img = new byte[0];
                var ProgramIdParam = new SqlParameter { ParameterName = "@ProgramId", Value = _Model.ProgramId };
                var SubProgramIdParam = new SqlParameter { ParameterName = "@SubProgramId", Value = _Model.SubProgramId };
                var AgencyIdParam = new SqlParameter { ParameterName = "@AgencyId", Value = _Model.AgencyId };
                var DistributionTypeParam = new SqlParameter { ParameterName = "@DistributionType", Value = _Model.DistributionType };
                var KindDistributionIdParam = new SqlParameter { ParameterName = "@KindDistributionId", Value = _Model.KindDistributionId };
                var AmountParam = new SqlParameter { ParameterName = "@Amount", Value = _Model.Amount };
                var HouseHoldNumberParam = new SqlParameter { ParameterName = "@HouseHoldNumber", Value = _Model.objHouseHoldModel.householdID1};
                var DescriptionParam = new SqlParameter { ParameterName = "@Descriptions", Value = _Model.Descriptions };
                var RemarksParam = new SqlParameter { ParameterName = "@Remarks", Value = _Model.Remarks };
                var CreatedDateParam = new SqlParameter { ParameterName = "@CreatedDate", Value = DateTime.Now };
                var CreatedByParam = new SqlParameter { ParameterName = "@CreatedBy", Value = _Model.AgencyId };
                var StatusParam = new SqlParameter { ParameterName = "@Status", Value = _Model.Status };

                //var ImagePathParam = new SqlParameter { ParameterName = "@ImagePath", Value = _Model.ServicesMain.ImagePath == null ? string.Empty : _Model.ServicesMain.ImagePath };
                //var IconImageParam = new SqlParameter { ParameterName = "@IconImage", Value = _Model.ServicesMain.IconImage == null ? img : _Model.ServicesMain.IconImage };
                //var ContactNumberparam = new SqlParameter { ParameterName = "@ContactNumber", Value = _Model.ServicesMain.ContactNumber == null ? string.Empty : _Model.ServicesMain.ContactNumber };

                var MessageParam = new SqlParameter
                {
                    ParameterName = "@Message",
                    DbType = DbType.String,
                    Size = 50,
                    Direction = System.Data.ParameterDirection.Output
                };


                var result = db.Database.ExecuteSqlCommand("exec InsertDistributions @ProgramId,@SubProgramId, @AgencyId,@DistributionType,@KindDistributionId,@Amount,@HouseHoldNumber,@Descriptions,@Remarks,@CreatedDate,@CreatedBy,@Status, @Message OUT",ProgramIdParam,SubProgramIdParam, AgencyIdParam, DistributionTypeParam, KindDistributionIdParam, AmountParam, HouseHoldNumberParam, DescriptionParam, RemarksParam, CreatedDateParam, CreatedByParam, StatusParam, MessageParam);
                msg = MessageParam.SqlValue.ToString();



                return msg;

            }
        }



        public List<HouseHoldModel> PopulateHouseholdDetailsHouseHoldId(DistributionDetailsModel model)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                List<HouseHoldModel> tempList;
                tempList = db.Database.SqlQuery<HouseHoldModel>("PopulateHouseholdDetailsHouseHoldId {0}", model.objHouseHoldModel.householdID1).ToList();
                return tempList;
            }
        }


    }
}