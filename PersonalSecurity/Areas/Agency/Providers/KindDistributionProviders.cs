﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PersonalSecurity.Areas.Agency.Models;
using System.Data.SqlClient;
using System.Data;

namespace PersonalSecurity.Areas.Agency.Providers
{
    public class KindDistributionProviders
    {


        public List<KindDistributionDetailModel> PopulateKindDistributionList()
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                List<KindDistributionDetailModel> KindDistributionList = new List<KindDistributionDetailModel>();
                KindDistributionList = db.Database.SqlQuery<KindDistributionDetailModel>("PopulateKindDistributionDetails").ToList();
                return KindDistributionList;
            }
        }


        public string InsertKindDistribution(KindDistributionDetailModel _Model)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                string msg = string.Empty;
                byte[] img = new byte[0];

                var KindNameParam = new SqlParameter { ParameterName = "@KindName", Value = _Model.KindName };
                var DescriptionParam = new SqlParameter { ParameterName = "@Description", Value = _Model.Description };
                var AgencyIdParam = new SqlParameter { ParameterName = "@AgencyId", Value = _Model.AgencyId };
                var EquivalentPriceParam = new SqlParameter { ParameterName = "@EquivalentPrice", Value = _Model.EquivalentPrice };
                var createdDateParam = new SqlParameter { ParameterName = "@CreatedDate", Value = DateTime.Now };
                var StatusParam = new SqlParameter { ParameterName = "@Status", Value = _Model.Status };

                //var ImagePathParam = new SqlParameter { ParameterName = "@ImagePath", Value = _Model.ServicesMain.ImagePath == null ? string.Empty : _Model.ServicesMain.ImagePath };
                //var IconImageParam = new SqlParameter { ParameterName = "@IconImage", Value = _Model.ServicesMain.IconImage == null ? img : _Model.ServicesMain.IconImage };
                //var ContactNumberparam = new SqlParameter { ParameterName = "@ContactNumber", Value = _Model.ServicesMain.ContactNumber == null ? string.Empty : _Model.ServicesMain.ContactNumber };

                var MessageParam = new SqlParameter
                {
                    ParameterName = "@Message",
                    DbType = DbType.String,
                    Size = 50,
                    Direction = System.Data.ParameterDirection.Output
                };


                var result = db.Database.ExecuteSqlCommand("exec InsertKindDistribution @KindName,@Description,@AgencyId,@EquivalentPrice,@CreatedDate,@Status, @Message OUT", KindNameParam, DescriptionParam, AgencyIdParam, EquivalentPriceParam, createdDateParam, StatusParam, MessageParam);
                msg = MessageParam.SqlValue.ToString();



                return msg;

            }
        }



        public string UpdateKindDistribution(KindDistributionDetailModel _Model)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                string msg = string.Empty;
                byte[] img = new byte[0];

                var KindDistributionIdParam = new SqlParameter { ParameterName = "@KindDistributionId", Value = _Model.KindDistributionId };
                var KindNameParam = new SqlParameter { ParameterName = "@KindName", Value = _Model.KindName };
                var DescriptionParam = new SqlParameter { ParameterName = "@Description", Value = _Model.Description };
                var AgencyIdParam = new SqlParameter { ParameterName = "@AgencyId", Value = _Model.AgencyId };
                var EquivalentPriceParam = new SqlParameter { ParameterName = "@EquivalentPrice", Value = _Model.EquivalentPrice };
                
                var MessageParam = new SqlParameter
                {
                    ParameterName = "@Message",
                    DbType = DbType.String,
                    Size = 50,
                    Direction = System.Data.ParameterDirection.Output
                };


                var result = db.Database.ExecuteSqlCommand("exec UpdateKindDistribution @KindDistributionId, @KindName,@Description,@AgencyId,@EquivalentPrice, @Message OUT", KindDistributionIdParam, KindNameParam, DescriptionParam, AgencyIdParam, EquivalentPriceParam,MessageParam);
                msg = MessageParam.SqlValue.ToString();



                return msg;

            }
        }


    }
}