﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PersonalSecurity.Areas.HouseHold.Models;
using System.ComponentModel.DataAnnotations;
namespace PersonalSecurity.Areas.Agency.Models
{
    public class DistributionDetailsModel
    {
        public int DistributionsId { get; set; }
        [Display(Name = "कार्यक्रम")]
        public int ProgramId { get; set; }
        [Display(Name = "उप कार्यक्रम")]
        public int SubProgramId { get; set; }
        [Display(Name = "निकाय")]
        public int AgencyId { get; set; }
        [Display(Name = "वितरणको प्रकार")]
        public int DistributionType { get; set; }
        [Display(Name = "वस्तु(जिन्सि)")]
        public int KindDistributionId { get; set; }
        [Display(Name = "घरमूलि")]
        public string HouseholdNumber { get; set; }
        [Display(Name = "रकम")]
        public decimal Amount { get; set; }
        [Display(Name = "विवरण")]
        public string Descriptions { get; set; }
        [Display(Name = "अन्य केहि भए लेख्नुहोस")]
        public string Remarks { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public bool Status { get; set; }
        [Display(Name = "वितरण मिति")]
        public DateTime? DistributedDate  { get; set; }

        public HouseHoldModel objHouseHoldModel { get; set; }       

        public List<DistributionDetailsModel> DistributionDetailsModelList { get; set; }

        public DistributionDetailsModel()
        {
            objHouseHoldModel = new HouseHoldModel();
            DistributionDetailsModelList = new List<DistributionDetailsModel>();
        }
    }
}