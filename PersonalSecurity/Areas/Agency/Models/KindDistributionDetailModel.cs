﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PersonalSecurity.Areas.Agency.Models
{
    public class KindDistributionDetailModel
    {

        public int KindDistributionId { get; set; }

        [Display(Name = "Kind(Inventory) Name")]
        public string KindName { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        public int AgencyId { get; set; }
        [Display(Name = "Status")]
        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }
        [Display(Name = "Market Rate(Approx)")]
        public decimal EquivalentPrice { get; set; }


        public KindDistributionDetailModel ObjKindDistributionDetailModel { get; set; }
        public List<KindDistributionDetailModel> KindDistributionDetailModelList { get; set; }
    }
}