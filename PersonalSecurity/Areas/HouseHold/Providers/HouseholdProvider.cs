﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PersonalSecurity.Areas.HouseHold.Models;

namespace PersonalSecurity.Areas.HouseHold.Providers
{
    public class HouseholdProvider
    {
        public List<HouseHoldModel> PopulateHouseHoldList(HouseHoldModel model)
        {
            using(PersoanlSecurityEntities db=new PersoanlSecurityEntities ())
            {
                List<HouseHoldModel> tempList;

                tempList = db.Database.SqlQuery<HouseHoldModel>("PopulateHouseHoldList {0},{1},{2},{3},{4},{5}",model.state,model.state_dist,model.lbcode_new,model.lb_ward,model.poor_popln,model.householdID1).ToList();           
                return tempList;
            }
        }

    }
}