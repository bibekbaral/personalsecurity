﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace PersonalSecurity.Areas.HouseHold.Models
{
    public class HouseHoldModel
    {
        [DisplayName("घरपरिवार क्रम सं")]
        public string householdID1 { get; set; }
        public double? recordid { get; set; }
        public double? serial_no { get; set; }
        public string belt_new { get; set; }
        public string dev_reg { get; set; }
        [DisplayName("प्रदेश")]
        public double? state { get; set; }
        public double? dist_code { get; set; }
        public double? dcode_new { get; set; }
        [DisplayName("जिल्ला")]
        public double? state_dist { get; set; }
          [DisplayName("District")]
        public string district { get; set; }
        [DisplayName("जिल्ला")]
        public string dist_Unicode { get; set; }
        public double? elec_cons_hr { get; set; }
        public double? elec_cons_sa { get; set; }
        public double? lb_code { get; set; }
        public double? lbcode_new { get; set; }
        public string local_body { get; set; }
        [DisplayName("नपा/गापा")]
        public string lbody_Unicode { get; set; }
        public string lbody_type { get; set; }
        public double? lb_popln { get; set; }
        public double? lb_area_sqkm { get; set; }
        public string lbody_centre { get; set; }
        [DisplayName("वडा नं.")]
        public double? lb_ward { get; set; }
        public double? vdc_mun_code { get; set; }
        public string vdc_mun_name { get; set; }
        public string vdc_mun_Unicode { get; set; }
        public double? vdc_code_cbs { get; set; }
        public string urb_rur { get; set; }
        public double? ward_no { get; set; }
        public double? new_vdcmun { get; set; }
        [DisplayName("नगरपलािका /गाउँपालिका")]
        public string new_vdc_mun { get; set; }
        [DisplayName("गाविस्/नपा")]
        public string new_vdc_mun_Unicode { get; set; }
        public string new_urbrur { get; set; }
        [DisplayName("वडा नं.")]
        public double? new_ward { get; set; }
        public double? enum_area { get; set; }
        public double? house_no { get; set; }
        public double? household_no { get; set; }
        public double? setlmnt_code { get; set; }
        public string setlmnt_English { get; set; }
        [DisplayName("गाँउ बस्ती")]
        public string setlmnt_Unicode { get; set; }
        public double? voter_no { get; set; }
        [DisplayName("नागरिकता प्रमाणपत्र नम्बर")]
        public string citizencp_no { get; set; }
        [DisplayName("जारी जिल्ला")]
        public string citzen_dcode { get; set; }
        public double? citizen_year { get; set; }
        public string name_1st_Nep { get; set; }
        public string name_mid_Nep { get; set; }
        public string name_sur_Nep { get; set; }
        public string name_1st_Eng { get; set; }
        public string name_mid_Eng { get; set; }
        public string name_sur_Eng { get; set; }
        public string fath_husband { get; set; }
        public string name_f_h_1st { get; set; }
        public string name_f_h_mid { get; set; }
        public string name_f_h_sur { get; set; }
        public string grand_fa_law { get; set; }
        public string name_gra_1st { get; set; }
        public string name_gra_mid { get; set; }
        public string name_gra_sur { get; set; }
        [DisplayName("लिङ्ग")]
        public string sex { get; set; }
        [DisplayName("२०७० को उमेर")]
        public double? age { get; set; }
        public string marital_stat { get; set; }
        public string ethnict_code { get; set; }
        public string landlin_phno { get; set; }
        public string mobile_phno { get; set; }
        public string educatn_head { get; set; }
        public string educa_spouse { get; set; }
        [DisplayName("परिवार संख्या")]
        public double? hhsize { get; set; }
        public double? child00_06 { get; set; }
        public double? child07_14 { get; set; }
        public double? adult15_59 { get; set; }
        public double? elderly60 { get; set; }
        public double? lit_06plus { get; set; }
        public double? lit_25plus { get; set; }
        public string study_pvtsch { get; set; }
        public double? male_pvtsch { get; set; }
        public double? fmle_pvtsch { get; set; }
        public string disability { get; set; }
        public double? dsblID_red { get; set; }
        public double? dsblID_blue { get; set; }
        public double? dsblID_yelow { get; set; }
        public double? dsblID_white { get; set; }
        public double? dsblID_none { get; set; }
        public string dsbl_mal1559 { get; set; }
        public double? dsbl_RedBlue { get; set; }
        public string chronic_ill { get; set; }
        public double? ill_cancer { get; set; }
        public double? ill_kidney { get; set; }
        public double? ill_heart { get; set; }
        public double? ill_mental { get; set; }
        public double? ill_other { get; set; }
        public string dmst_servant { get; set; }
        public string absentee { get; set; }
        public double? absent_male { get; set; }
        public double? absent_femal { get; set; }
        public double? absent_total { get; set; }
        public string house_tenure { get; set; }
        public double? no_room { get; set; }
        public string type_foundtn { get; set; }
        public string type_wall { get; set; }
        public string type_roof { get; set; }
        public string type_toilet { get; set; }
        public string light_fuel { get; set; }
        public string cook_fuel { get; set; }
        public string cook_stove { get; set; }
        public string drink_water { get; set; }
        public string landln_phone { get; set; }
        public string mobile_phone { get; set; }
        public string tv { get; set; }
        public string cabledish_tv { get; set; }
        public string internet { get; set; }
        public string computer { get; set; }
        public string motorcycle { get; set; }
        public string vehicle { get; set; }
        public string rickshaw { get; set; }
        public string wash_machine { get; set; }
        public string fridge { get; set; }
        public string solar_heater { get; set; }
        public string ac { get; set; }
        public string electric_fan { get; set; }
        public string tractor { get; set; }
        public string pumpset { get; set; }
        public string deep_boring { get; set; }
        public string ox_buf_cart { get; set; }
        public string farm_land { get; set; }
        public string unit_land { get; set; }
        public string lowland2_B { get; set; }
        public string lowland2_K { get; set; }
        public string lowland2_D { get; set; }
        public string lowland2_R { get; set; }
        public string lowland2_A { get; set; }
        public string lowland2_P { get; set; }
        public string lowland1_B { get; set; }
        public string lowland1_K { get; set; }
        public string lowland1_D { get; set; }
        public double? lowland1_R { get; set; }
        public double? lowland1_A { get; set; }
        public double? lowland1_P { get; set; }
        public string upland_B { get; set; }
        public string upland_K { get; set; }
        public string upland_D { get; set; }
        public double? upland_R { get; set; }
        public double? upland_A { get; set; }
        public double? upland_P { get; set; }
        public string pcrop_pond_B { get; set; }
        public string pcrop_pond_K { get; set; }
        public string pcrop_pond_D { get; set; }
        public double? pcrop_pond_R { get; set; }
        public double? pcrop_pond_A { get; set; }
        public double? pcrop_pond_P { get; set; }
        public double? agriarea_ha { get; set; }
        public string livestock { get; set; }
        public string cattle { get; set; }
        public double? no_cattle { get; set; }
        public string buffalo { get; set; }
        public double? no_buffalo { get; set; }
        public string yak { get; set; }
        public string no_yak { get; set; }
        public string horse { get; set; }
        public string no_horse { get; set; }
        public string sheep_goat { get; set; }
        public string no_sheepgoat { get; set; }
        public string pig { get; set; }
        public double? no_pig { get; set; }
        public string chicken { get; set; }
        public double? no_chicken { get; set; }
        public string duck { get; set; }
        public string no_duck { get; set; }
        public string salary_job { get; set; }
        public string sector_job { get; set; }
        public string month_salary { get; set; }
        public string business_job { get; set; }
        public double? income_source { get; set; }
        public string inc_ag_lvstk { get; set; }
        public string inc_nonag_bz { get; set; }
        public string inc_service { get; set; }
        public string inc_pension { get; set; }
        public string inc_remitans { get; set; }
        public string inc_dwage { get; set; }
        public string inc_strade { get; set; }
        public string inc_charity { get; set; }
        public string inc_other { get; set; }
        public string income_year { get; set; }
        public string income_stat { get; set; }
        public string expens_month { get; set; }
        public string food_adeqecy { get; set; }
        public double? sfood_unedqc { get; set; }
        public string unedq_trade { get; set; }
        public string unedq_labor { get; set; }
        public string unedq_loan { get; set; }
        public string unedq_other { get; set; }
        public string remittance { get; set; }
        public double? remit_amount { get; set; }
        public string memb_co_op { get; set; }
        public string memship_co_op { get; set; }
        public string co_op_saving { get; set; }
        public string co_op_agri { get; set; }
        public string co_op_dairy { get; set; }
        public string co_op_multi { get; set; }
        public string co_op_other { get; set; }
        public string loan_co_op { get; set; }
        public string loan_fin_ins { get; set; }
        public string mem_dom_work { get; set; }
        public string mem_loc_body { get; set; }
        public string family_poor { get; set; }
        public double? pmt_score { get; set; }
        [DisplayName("गरिव प्रकार")]
        public string poor_popln { get; set; }
        public string remarks { get; set; }


        [DisplayName("घरमूलिको नाम थर")]
        public string FullName
        {
            get {

                string name = "";
                if (string.IsNullOrEmpty(name_mid_Nep))
                {
                    name = name_1st_Nep + " " + name_sur_Nep;
                }
                else { name=name_1st_Nep + " " + name_mid_Nep + " " + name_sur_Nep;}
                return name;
            
            
            }
        }

        [DisplayName("बाबु/पतिको नामथर")]
        public string FatherHusbandFullName
        {
            get
            {

                string name = "";
                if (string.IsNullOrEmpty(name_f_h_mid))
                {
                    name = name_f_h_1st + " " + name_f_h_sur;
                }
                else { name = name_f_h_1st + " " + name_f_h_mid + " " + name_f_h_sur; }
                return name;


            }
        }

        public List<HouseHoldModel> HouseHoldModelList { get; set; }

        public HouseHoldModel()
        {
            HouseHoldModelList = new List<HouseHoldModel>();
        }
    }
}