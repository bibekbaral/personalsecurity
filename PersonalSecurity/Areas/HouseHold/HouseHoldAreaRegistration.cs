﻿using System.Web.Mvc;

namespace PersonalSecurity.Areas.HouseHold
{
    public class HouseHoldAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HouseHold";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HouseHold_default",
                "HouseHold/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
