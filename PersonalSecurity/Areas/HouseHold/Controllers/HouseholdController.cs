﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PersonalSecurity.Areas.HouseHold.Providers;
using PersonalSecurity.Areas.HouseHold.Models;
namespace PersonalSecurity.Areas.HouseHold.Controllers
{
    public class HouseholdController : Controller
    {
        //
        // GET: /HouseHold/Household/

        HouseholdProvider providerObj = new HouseholdProvider();
        public ActionResult HouseholdList()
        {
            HouseHoldModel model = new HouseHoldModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult HouseholdList(HouseHoldModel model)
        {
            
            model.HouseHoldModelList = providerObj.PopulateHouseHoldList(model);
            return View(model);
        }

    }
}
