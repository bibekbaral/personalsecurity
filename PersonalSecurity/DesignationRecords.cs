//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PersonalSecurity
{
    using System;
    using System.Collections.Generic;
    
    public partial class DesignationRecords
    {
        public int DesignationSetupId { get; set; }
        public string DesignationNameEng { get; set; }
        public string DesignationNameNep { get; set; }
        public bool Status { get; set; }
        public int DesignationFor { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    }
}
