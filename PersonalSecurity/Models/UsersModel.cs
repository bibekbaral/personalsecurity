﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonalSecurity.Models
{
    public class UsersModel
    {

        public int UserRoleId { get; set; }
        public webAction webActionModel { get; set; }
        public webCustomRole webCustomRoleModel { get; set; }

        public List<webAction> webActionList { get; set; }
        public List<webCustomRole> webCustomRoleList { get; set; }
      
    }

    public class webAction
    {
        public int ActionId { get; set; }
        public int ControllerId { get; set; }
        public string ActionName { get; set; }
        public string DisplayTitle { get; set; }
        public bool Status { get; set; }
        public bool CheckStatus { get; set; }
    }



    public class webCustomRole
    {
        public int CustomRoleId { get; set; }
        public int UserType { get; set; }
        public int RoleId { get; set; }
        public int ActionId { get; set; }
        public bool Status { get; set; }
    }
}