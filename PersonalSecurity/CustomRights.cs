﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;



namespace PersonalSecurity.AppAttribute
{
    public class ValidateAction : ActionFilterAttribute
    {        
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                HttpContext cc = HttpContext.Current;
                bool _result = false;
                string _ControllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToString();
                string _ActionName = filterContext.ActionDescriptor.ActionName.ToString();                            
                int userid = WebSecurity.GetUserId(cc.User.Identity.Name);
                int roleid = db.Database.SqlQuery<int>("Select RoleId from webpages_UsersInRoles where UserId='" + userid + "'").FirstOrDefault();
                int usertype = db.Database.SqlQuery<int>("Select UserType from UserProfile where UserId='" + userid + "'").FirstOrDefault();

                if (_ControllerName != "AjaxCall")
                {
                     //string aa = "GetUserRights '" + _ActionName + "','" + _ControllerName + "','" + userid + "','" + usertype + "','" + roleid + "'";
                    var _action = db.Database.SqlQuery<int>("GetUserRights '" + _ActionName + "','" + _ControllerName + "','" + userid + "','" + usertype + "','" + roleid + "'").FirstOrDefault();

                    if (Convert.ToInt32(_action) > 0)
                    {
                        _result = true;
                    }
                    else if (_ControllerName == "AjaxRequest")
                    {
                        _result = true;
                    }
                    else
                    {
                        System.Web.Routing.RouteValueDictionary rd = new System.Web.Routing.RouteValueDictionary(
                            new { Controller = "Home", Action = "AccessDenied" });
                        filterContext.Result = new RedirectToRouteResult(rd);
                    }
                }
                base.OnActionExecuting(filterContext);
            }

        }

    


    }
}