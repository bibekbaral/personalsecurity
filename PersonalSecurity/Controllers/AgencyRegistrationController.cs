﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PersonalSecurity.Areas.Admin.Models;
using PersonalSecurity.Areas.Admin.Providers;

namespace PersonalSecurity.Controllers
{
    public class AgencyRegistrationController : Controller
    {
        //
        // GET: /AgencyRegistration/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult AgencyRegister()
        {
            AgencySetupModel model = new AgencySetupModel();
            model.ProvincesId = 1;
            model.ProvincesDistrictId = 1;
            return View(model);
        }

        [HttpPost]
        public ActionResult AgencyRegister(AgencySetupModel model)
        {
            
            AgencySetupProviders pro = new AgencySetupProviders();
            model.Status = false;
            //check agency name or email already exist 
            if (CommonUtilities.IsUserNameAlreadyExist(model.EmailId))
            {
                ViewBag.EmailAlreadyExist = "Email already used in system.";
                return View(model);
            }

            if (pro.insertAgencySetup(model)=="Saved Successfully")
            {
                return RedirectToAction("Registered");
            }
            else
            {
                return View(model);
            }
            
        }

        public ActionResult Registered()
        {

            return View();
        }

    }
}
