﻿using PersonalSecurity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;


namespace PersonalSecurity
{
    public class CommonUtilities
    {

        public static int GetUserID(string username)
        {
            int i = WebSecurity.GetUserId(username);
            return i;
        }

        public static int GetCurrentLoginUserId()
        {
            var UserContext = new UsersContext();
            var userId = WebSecurity.GetUserId(HttpContext.Current.User.Identity.Name);//user profile user id
            return userId;
        }


        public static int? GetCurrentLoginAgencyId()
        {
            var UserContext = new UsersContext();
            int userId = WebSecurity.GetUserId(HttpContext.Current.User.Identity.Name);
            var AgencyId = UserContext.UserProfiles.SingleOrDefault(x => x.UserId == userId).AgencyId;
            return AgencyId;
        }




        public static int GetCurrentLoginUserType()
        {

            var UserContext = new UsersContext();
            var userId = WebSecurity.GetUserId(HttpContext.Current.User.Identity.Name);
            var user = UserContext.UserProfiles.SingleOrDefault(u => u.UserName == HttpContext.Current.User.Identity.Name);
            int UserType = user.UserType;
            return UserType;
        }

        public static bool IsUserNameAlreadyExist(string UserName)
        {
            var UserContext = new UsersContext();
            UserName = UserName.Trim();
            bool IfUserNameExist = UserContext.UserProfiles.Any(x => x.UserName == UserName);
            if (IfUserNameExist)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        public static SelectList GetDesignationList()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                return new SelectList(ent.DesignationRecords.ToList(), "DesignationSetupId", "DesignationNameEng");
            }
        }




        public static SelectList GetMaritialRecordsList()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                return new SelectList(ent.MartialStatusRecords.ToList(), "MartialStatusId", "StatusNameEng");
            }
        }


        public static SelectList GetGenderRecordsList()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                return new SelectList(ent.GenderRecords.ToList(), "GenderId", "GenderEnglish");
            }
        }



        public static SelectList GetDepartmentList()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                return new SelectList(ent.DepartmentRecords.ToList(), "DepartmentRecordsId", "DepartmentNameEng");
            }
        }

        public static SelectList GetUserType()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                return new SelectList(ent.Database.SqlQuery<RoleModel>("Select UserTypeId,UserTypeName from UserType").ToList(), "UserTypeId", "UserTypeName");
            }
        }

        public static SelectList GetprovincesNepal()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                return new SelectList(ent.provincesSetup.ToList(), "provincesId", "provincesName");
            }
        }

        public static string GetProvinecesNepalById(int ProId)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var result = ent.provincesSetup.Where(x => x.provincesId == ProId).SingleOrDefault();
                return result.provincesName;
            }
        }

        public static SelectList GetprovincesDistrictOfNepal(int ProvincesId)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {

                List<SelectListItem> ddlList = new List<SelectListItem>();

                var collection = ent.Database.SqlQuery<PersonalSecurity.Areas.Admin.Controllers.AjaxCallController.ReturnViewmodel>(@"select DistrictCode as PrimaryKeyId,NameNepali as ValueName From District where StateId='" + ProvincesId + "' ").ToList();
                //var collection = ent.ProvincesDistrictSetup.Where(x => x.ProvincesId == id);

                foreach (var item in collection)
                {
                    ddlList.Add(new SelectListItem { Text = item.ValueName.ToString(), Value = item.PrimaryKeyId.ToString() });
                }
                var ddlSelectOptionList = ddlList;
                return new SelectList(ddlList.ToList(), "Value", "Text");

                //return new SelectList(ent.ProvincesDistrictSetup.Where(x => x.ProvincesId == ProvincesId).ToList(), "ProvincesDistrictId", "DistrictName");
            }
        }

        public static string GetProvinecesDistrictByDistrictId(int DistrictId)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var result = ent.ProvincesDistrictSetup.Where(x => x.ProvincesDistrictId == DistrictId).SingleOrDefault();
                return result.DistrictName;
            }
        }


        public static string GetProvinecesDistrictByDistrictIdNew(int DistricId)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                string DistrictName = ent.Database.SqlQuery<string>(@"select NameNepali as ValueName from District where DistrictCode='" + DistricId + "' ").FirstOrDefault();
                return DistrictName;
            }
        }

        public static IEnumerable<SelectListItem> GetVDCMunicipalityType()
        {
            return new SelectList(new[]
            {
                new {Id="1",Value="महानगरपालिका"},
                new {Id="2",Value="उपमहानगरपालिका"},
                new {Id="4",Value="नगरपालिका"},
                new {Id="3",Value="गाउँपालिका"},
            }, "Id", "Value");

        }

        public static string GetVDCMunicipalityTypeByID(int TypeId)
        {
            string GetVDCMunicipalityType = "";
            if (TypeId == 1)
            {
                GetVDCMunicipalityType = "महानगरपालिका";

            }
            else if (TypeId == 2)
            {
                GetVDCMunicipalityType = "उपमहानगरपालिका";
            }
            else if (TypeId == 3)
            {
                GetVDCMunicipalityType = "गाउँपालिका";
            }
            else if (TypeId == 4)
            {
                GetVDCMunicipalityType = "नगरपालिका";
            }

            return GetVDCMunicipalityType;
        }

        public static SelectList GetVDCNPTitleOfProvinces()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                return new SelectList(ent.provincesVDCDetails.ToList(), "ProvincesVDCDetailsID", "VDCName");
            }
        }

        public static SelectList GetVDCNPTitleOfProvincesByProvincesId(int ProvincesId, int DistrictId)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                List<SelectListItem> ddlList = new List<SelectListItem>();
                // var collection = ent.provincesVDCDetails.Where(x => x.ProvincesDistrictId == id);
                var collection = ent.Database.SqlQuery<PersonalSecurity.Areas.Admin.Controllers.AjaxCallController.ReturnViewmodel>(@"select RuralMunicipalityId as PrimaryKeyId,NameNepali as ValueName From RuralMunicipality where DistrictId='" + DistrictId + "' and StateId='" + ProvincesId + "'").ToList();
                foreach (var item in collection)
                {
                    ddlList.Add(new SelectListItem { Text = item.ValueName, Value = item.PrimaryKeyId.ToString() });
                }
                var ddlSelectOptionList = ddlList;
                return new SelectList(ddlList, "Value", "Text");

                //return new SelectList(ent.provincesVDCDetails.Where(x => x.ProvincesId == ProvincesId).ToList(), "ProvincesVDCDetailsID", "VDCName");
            }
        }

        public static SelectList GetProvincesVDCNPTitle(int DistrictId)
        {

            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                List<SelectListItem> ddlList = new List<SelectListItem>();
                return new SelectList(ent.Database.SqlQuery<PersonalSecurity.Areas.Household.Controllers.AjaxCallController.ReturnViewmodel>(@"select RuralMunicipalityId as PrimaryKeyId,NameNepali as ValueName From RuralMunicipality where DistrictId='" + DistrictId + "'").ToList(), "PrimaryKeyId", "ValueName");
            }

        }


        public static SelectList GetKindDistributionsByAgencyId(int AgencyId)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                List<SelectListItem> ddlList = new List<SelectListItem>();
                ddlList.Add(new SelectListItem { Text = "--छान्नुहोस--", Value = "0", Selected = true });
                // var collection = ent.provincesVDCDetails.Where(x => x.ProvincesDistrictId == id);
                var collection = ent.Database.SqlQuery<PersonalSecurity.Areas.Admin.Controllers.AjaxCallController.ReturnViewmodel>(@"select KindDistributionId as PrimaryKeyId,KindName as ValueName From KindDistributionDetails where AgencyId='" + AgencyId + "'").ToList();
                foreach (var item in collection)
                {
                    ddlList.Add(new SelectListItem { Text = item.ValueName, Value = item.PrimaryKeyId.ToString() });
                }
                var ddlSelectOptionList = ddlList;
                return new SelectList(ddlList, "Value", "Text");


            }
        }


        public static SelectList GetProgramList()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                return new SelectList(ent.ProgramSetup.ToList(), "ProgramSetupId", "ProgramName");
            }
        }

        public static SelectList GetSubProgramList()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                int CurrentUserId = CommonUtilities.GetCurrentLoginUserId();
                return new SelectList(ent.SubProgramSetup.ToList().Where(x => x.CreatedBy == CurrentUserId), "SubProgramSetupId", "SubProgramName");
            }
        }


        public static string GetProgramNameByProgramId(int ProgramId)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var result = ent.ProgramSetup.SingleOrDefault(x => x.ProgramSetupId == ProgramId);
                if (result != null)
                {
                    return result.ProgramName;
                }
                else
                    return "---";
            }
        }

        public static string GetSubProgramNameBySubProgramId(int SubProgramId)
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                var result = ent.SubProgramSetup.SingleOrDefault(x => x.SubProgramSetupId == SubProgramId);
                if (result != null)
                {
                    return result.SubProgramName;
                }
                else
                    return "---";
            }
        }



        public class RoleModel
        {
            public int RoleId { get; set; }
            public string RoleName { get; set; }
            public string USER_TYPE_CD { get; set; }
            public string DESC_ENG { get; set; }
            public int UserTypeId { get; set; }
            public string UserTypeName { get; set; }
        }

        //webpage role
        public static SelectList GetGroup()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                return new SelectList(ent.Database.SqlQuery<RoleModel>("Select RoleId,RoleName from webpages_Roles").ToList(), "RoleId", "RoleName");
            }
        }

        public static List<UserRoleModel> GetUserRoleList()
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                List<UserRoleModel> modellist = new List<UserRoleModel>();
                modellist = db.Database.SqlQuery<UserRoleModel>("GetUserRole").ToList();
                if (modellist.Count < 1)
                    modellist = new List<UserRoleModel>();
                return modellist;

            }
        }

        public class UserRoleModel
        {
            public int RoleId { get; set; }
            public string RoleName { get; set; }
        }

        public static bool IfExistsRole(int id)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                bool check = false;
                check = db.web_CustomRole.Any(c => c.RoleId == id);
                return check;
            }
        }

        public static List<web_Controller> PopulateControllerList()
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                List<web_Controller> modellist = new List<web_Controller>();
                modellist = db.Database.SqlQuery<web_Controller>("PopulateControllerList").ToList();
                if (modellist.Count < 1)
                    modellist = new List<web_Controller>();
                return modellist;
            }
        }

        public static IEnumerable<SelectListItem> GetDistributionHelpType()
        {
            return new SelectList(new[]
            {
                new {Id="1",Value="रकम"},
                new {Id="2",Value="वस्तु (जिन्सि)"},
                new {Id="3",Value="सेवा"}

                
            }, "Id", "Value");

        }


        public static string GetMainMenuLabel()
        {
            return "मुख्य सूची";
        }
        public static string GetAddLabel()
        {
            return "नयाँ";
        }

        public static string GetListLabel()
        {
            return "सूची";
        }

        public static string GetTotalLabel()
        {
            return "जम्मा संख्या";
        }


        public static string GetSubmitLabel()
        {
            return "पेश गर्नुहोस्";
        }
        public static string GetEditLabel()
        {
            return "सम्पादन गर्नुहोस्";
        }

        public static string GetSearchLabel()
        {
            return "खोज्जनुहोस";
        }

        public static SelectList GetProvinces()
        {
            using (PersoanlSecurityEntities ent = new PersoanlSecurityEntities())
            {
                return new SelectList(ent.Database.SqlQuery<SelectListModel>("GETPROVINCES").ToList(), "Id", "Value");
            }
        }


        public class SelectListModel
        {
            public int Id { get; set; }
            public string Value { get; set; }
        }

        public static IEnumerable<SelectListItem> GetWardNo()
        {
            return new SelectList(new[]
            {
                new {Id="1",Value="१"},
                new {Id="2",Value="२"},
                new {Id="3",Value="३"},
                new {Id="4",Value="४"},
                new {Id="5",Value="५"},
                new {Id="6",Value="६"},
                new {Id="7",Value="७"},
                new {Id="8",Value="८"},
                new {Id="9",Value="९"},
                new {Id="10",Value="१०"},
                new {Id="11",Value="११"},
                new {Id="12",Value="१२"},
                new {Id="13",Value="१३"},
                new {Id="14",Value="१४"},
                new {Id="15",Value="१५"},
                new {Id="16",Value="१६"},
                new {Id="17",Value="१७"},
                new {Id="18",Value="१८"},
                new {Id="19",Value="१९"},
                new {Id="20",Value="२०"},
                new {Id="21",Value="२१"},
                new {Id="22",Value="२२"},
                new {Id="23",Value="२३"},
                new {Id="24",Value="२४"},
                new {Id="25",Value="२५"},
                new {Id="26",Value="२६"},
                new {Id="27",Value="२७"},
                new {Id="28",Value="२८"},
                new {Id="29",Value="२९"},
                new {Id="30",Value="३०"},
                new {Id="31",Value="३१"},
                new {Id="32",Value="३२"},
                new {Id="33",Value="३३"},
                new {Id="34",Value="३४"},
                new {Id="35",Value="३५"},
           
            }, "Id", "Value");

        }

        public static string GetNeplaiNumber(int englishnumber)
        {
            using (PersoanlSecurityEntities db = new PersoanlSecurityEntities())
            {
                string nepalinumber = string.Empty;
                nepalinumber = db.Database.SqlQuery<string>("GetNepaliNumber " + englishnumber + "").FirstOrDefault();
                return nepalinumber;
            }
        }


        public static string GetGender(string id)
        {
            if (id == "1")
                return "पुरूष";
            else if (id == "2")
                return "महिला";
            else
                return "अन्य";
        }


        public static string GetPoorTypeinNepali(string id)
        {
            if (id == "1")
                return "अति गरिब";
            else if (id == "2")
                return "मध्यम गरिब";
            else if (id == "3")
                return "सा.गरिब";
            else if (id == "4")
                return "";
            else
                return "";
        }

        public static IEnumerable<SelectListItem> GetPoorType()
        {

            return new SelectList(new[]
            {
                new {Id="1",Value="अति गरिब"},
                new {Id="2",Value="मध्यम गरिब"},
                new {Id="3",Value="सा.गरिब"},
                new {Id="4",Value="गैर गरिब"},
                
            }, "Id", "Value");

        }
        public static IEnumerable<SelectListItem> GetPoorTypeForAgency()
        {
            return new SelectList(new[]
            {
                new {Id="1",Value="अति गरिब"},
                new {Id="2",Value="मध्यम गरिब"},
                new {Id="3",Value="सा.गरिब"},
             
                
            }, "Id", "Value");

        }
        public static string GetPoorTypecolor(string id)
        {
            if (id == "1")
                return "red";
            else if (id == "2")
                return "yellow";
            else if (id == "3")
                return "blue";
            else if (id == "4")
                return "";
            else
                return "";
        }
    }
}