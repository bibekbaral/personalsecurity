﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PersonalSecurity
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Objects;
    using System.Data.Objects.DataClasses;
    using System.Linq;
    
    public partial class PersoanlSecurityEntities : DbContext
    {
        public PersoanlSecurityEntities()
            : base("name=PersoanlSecurityEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<UserType> UserType { get; set; }
        public DbSet<DesignationRecords> DesignationRecords { get; set; }
        public DbSet<GenderRecords> GenderRecords { get; set; }
        public DbSet<MartialStatusRecords> MartialStatusRecords { get; set; }
        public DbSet<EmployeeRecords> EmployeeRecords { get; set; }
        public DbSet<DepartmentRecords> DepartmentRecords { get; set; }
        public DbSet<ProvincesDistrictSetup> ProvincesDistrictSetup { get; set; }
        public DbSet<provincesSetup> provincesSetup { get; set; }
        public DbSet<provincesVDCDetails> provincesVDCDetails { get; set; }
        public DbSet<web_Action> web_Action { get; set; }
        public DbSet<web_Controller> web_Controller { get; set; }
        public DbSet<web_CustomRole> web_CustomRole { get; set; }
        public DbSet<ProgramSetup> ProgramSetup { get; set; }
        public DbSet<SubProgramSetup> SubProgramSetup { get; set; }
        public DbSet<KindDistributionDetails> KindDistributionDetails { get; set; }
        public DbSet<AgencyGeographicCoverage> AgencyGeographicCoverage { get; set; }
        public DbSet<SubProgramGeoDetails> SubProgramGeoDetails { get; set; }
    
        public virtual ObjectResult<SP_MAIN_USERACTION_Result> SP_MAIN_USERACTION()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_MAIN_USERACTION_Result>("SP_MAIN_USERACTION");
        }
    
        public virtual int SP_USERACTION()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_USERACTION");
        }
    }
}
